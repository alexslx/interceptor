# SMS & Call Interceptor

With SMS & Call Interceptor you can make a private contact list and manage your phone interation with these contacts.

## Features

* Application
	* Hide it from drawer
	* Quick access from Android Dialer with special codes.
* Contacts
	* Hide contact from Android Contact List
* Messages (SMS/MMS)
	* Intercept and hide messages received.
	* Store messages into a private vault.
	* Display a custom notification to alert you of a new intercepted message.
* Calls
	* Intercept and hide incomming calls.
	* Store a private history with missed events.
	* Display a custom notification to alert you of a new intercepted call.
	* Block outgoing call (You can unblock with a special code)

## Special Codes

You can access features from SMS & Call Interceptor with our access codes. By default, our access code is 8520 but you can change it from settings menu.
