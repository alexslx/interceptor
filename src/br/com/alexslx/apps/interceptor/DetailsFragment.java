/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.interceptor;

import java.util.ArrayList;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import br.com.alexslx.apps.interceptor.adapters.AdapterContactHistory;
import br.com.alexslx.apps.interceptor.adapters.AdapterContactMessages;
import br.com.alexslx.apps.interceptor.adapters.AdapterCustomListView;
import br.com.alexslx.apps.interceptor.models.Contact;
import br.com.alexslx.apps.interceptor.models.ContactHistory;
import br.com.alexslx.apps.interceptor.models.ContactMessage;
import br.com.alexslx.apps.interceptor.models.CustomListItem;
import br.com.alexslx.apps.interceptor.models.VaultContact;
import br.com.alexslx.apps.interceptor.utils.L;
import br.com.alexslx.apps.interceptor.utils.VaultUtils;

public class DetailsFragment extends Fragment implements OnItemClickListener, OnItemLongClickListener, android.content.DialogInterface.OnClickListener, OnClickListener
{
	/**
	 * The fragment argument representing the section number for this fragment.
	 */
	static final String					ARG_SECTION_NUMBER		= "section_number";

	private Contact						cContact				= null;
	private VaultContact				vContact				= null;

	private int							tabPosition				= 0;

	// Message Dialog
	private int							messagePhoneIndex		= 0;
	private boolean						messageDialogChecked	= false;
	private String						messageData				= "";

	private AdapterContactMessages		adapterMessages;
	private AdapterContactHistory		adapterHistory;
	private AdapterCustomListView		adapterInfoList;
	private ArrayList<ContactMessage>	messages;
	private ArrayList<ContactHistory>	history;
	private ArrayList<CustomListItem>	infos;
	private ListView					listView1;
	private ListView					listView2;
	private ListView					listView3;

	contactActivityInterface			mCallback;

	public DetailsFragment()
	{

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View myFragmentView = null;
		this.tabPosition = getArguments().getInt(ARG_SECTION_NUMBER);
		this.cContact = ((ApplicationHelper) getActivity().getApplication()).getContact();
		this.vContact = this.cContact.getContact();
		this.setRetainInstance(true);

		switch (tabPosition)
		{
			case 1:
				myFragmentView = inflater.inflate(R.layout.contact_infos, container, false);
				break;
			case 2:
				myFragmentView = inflater.inflate(R.layout.contact_messages, container, false);
				break;
			case 3:
				myFragmentView = inflater.inflate(R.layout.contact_history, container, false);
				break;
			default:
				L.e("TabIndex error: " + tabPosition);
				break;
		}

		return myFragmentView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		if (this.vContact == null)
			return;

		switch (tabPosition)
		{
			case 1:
				//
				listView1 = (ListView) getView().findViewById(R.id.contactInfosList);
				loadContactInfo();
				break;
			case 2:
				listView2 = (ListView) getView().findViewById(R.id.messagesList);
				loadMessagesLayout();
				loadMessages();
				break;
			case 3:
				listView3 = (ListView) getView().findViewById(R.id.historyList);
				TextView historySize = (TextView) getView().findViewById(R.id.historySize);
				loadHistory();

				Resources res = getResources();
				historySize.setText(res.getQuantityString(R.plurals.contact_history_size, adapterHistory.getCount(), adapterHistory.getCount()));
				break;
		}

	}

	private void loadMessagesLayout()
	{
		final EditText inputText = (EditText) getView().findViewById(R.id.sendTextMessage);
		Button sendTextButton = (Button) getView().findViewById(R.id.sendTextButton);

		sendTextButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v)
			{
				if (inputText.getText().toString().isEmpty())
					return;

				sendTextMessage(inputText.getText().toString(), false);
			}
		});

		sendTextButton.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v)
			{
				if (inputText.getText().toString().isEmpty())
					return false;

				sendTextMessage(inputText.getText().toString(), true);
				inputText.setText("");
				return false;
			}
		});
	}

	protected void sendTextMessage(String message, boolean forceAsk)
	{
		if (this.vContact.phones.size() < 1)
			return;

		if (this.vContact.phones.size() > 1)
		{
			ArrayAdapter<CharSequence> phones = new ArrayAdapter<CharSequence>(getActivity(), android.R.layout.select_dialog_singlechoice);
			for (HashMap<String, String> data : this.vContact.phones)
			{
				phones.add(data.get(ContactsContract.Data.DATA1));
			}

			messagePhoneIndex = this.vContact.optMessagePhoneIndex;

			if (forceAsk || this.vContact.optAskMessagePhone)
			{
				LayoutInflater mInflater = LayoutInflater.from(getActivity());
				View rememberLayout = mInflater.inflate(R.layout.dialog_remember, null);
				CheckBox rememberCheck = (CheckBox) rememberLayout.findViewById(R.id.checkRemember);
				rememberCheck.setOnClickListener(this);
				rememberCheck.setChecked(this.vContact.optAskMessagePhone);

				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder.setTitle(this.vContact.getDisplayName());
				builder.setView(rememberLayout);
				builder.setSingleChoiceItems(phones, this.vContact.optMessagePhoneIndex, this);
				builder.setNegativeButton(getString(R.string.general_cancel), null);
				builder.setPositiveButton(getString(R.string.general_send), this);
				builder.show();

				messageData = message;
			}
			else
			{
				sendMessage(message, this.vContact, messagePhoneIndex);
				((EditText) getView().findViewById(R.id.sendTextMessage)).setText("");
			}
		}
		else
		{
			sendMessage(message, this.vContact, messagePhoneIndex);
			((EditText) getView().findViewById(R.id.sendTextMessage)).setText("");
		}
	}

	public void sendMessage(String message, VaultContact c, int phoneIndex)
	{
		if (c.phones.size() < 1)
			return;

		if (c.phones.size() <= phoneIndex)
			return;

		HashMap<String, String> data = new HashMap<String, String>();

		data = c.phones.get(phoneIndex);
		String toNumber = data.get(ContactsContract.Data.DATA1);

		ContactMessage msg = ContactMessage.newMessageEntry(getActivity(), this.vContact.getID(), message, false, false);
		msg.setToNumber(toNumber);
		msg.save();

		addMessageToList(msg);
	}

	public void addMessageToList(ContactMessage msg)
	{
		if (msg == null)
			return;

		messages.add(msg);
		adapterMessages.notifyDataSetChanged();
	}

	public void addHistory(String number, long date, int type)
	{
		ContactHistory data = new ContactHistory(getActivity());
		data.setContactId(this.vContact.getID());
		data.setDate(date);
		data.setNumber(number);
		data.setType(type);
		data.save();

		addHistoryToList(data);
	}

	public void addHistoryToList(ContactHistory data)
	{
		if (history == null)
			return;

		history.add(data);
		adapterHistory.notifyDataSetChanged();
	}

	public void configureListView(ListView listView)
	{
		listView.setCacheColorHint(Color.TRANSPARENT);
		listView.setOnItemClickListener(this);
		listView.setOnItemLongClickListener(this);
		listView.setLongClickable(true);
		listView.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
	}

	public interface contactActivityInterface
	{
		public ArrayList<ContactMessage> loadMessages();

		public ArrayList<ContactHistory> loadHistory();
	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);

		try
		{
			mCallback = (contactActivityInterface) activity;
		}
		catch (Exception e)
		{

		}
	}

	public void loadMessages()
	{
		if (messages == null)
			messages = mCallback.loadMessages();

		if (listView2 == null)
			listView2 = (ListView) getView().findViewById(R.id.messagesList);

		adapterMessages = new AdapterContactMessages(getActivity(), messages);
		listView2.setAdapter(adapterMessages);
		configureListView(listView2);

		adapterMessages.notifyDataSetChanged();
	}

	public void loadHistory()
	{
		if (history == null)
			history = mCallback.loadHistory();

		if (listView3 == null)
			listView3 = (ListView) getView().findViewById(R.id.historyList);

		adapterHistory = new AdapterContactHistory(getActivity(), history);
		listView3.setAdapter(adapterHistory);
		configureListView(listView3);

		adapterHistory.notifyDataSetChanged();
	}

	public void loadContactInfo()
	{
		if (listView1 == null)
			listView1 = (ListView) getView().findViewById(R.id.contactInfosList);

		infos = new ArrayList<CustomListItem>();
		vContact.fillListView(getActivity(), infos);

		adapterInfoList = new AdapterCustomListView(getActivity(), infos);
		listView1.setAdapter(adapterInfoList);
		configureListView(listView1);
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3)
	{
		switch (tabPosition)
		{
			case 2:
				// ContactMessage msg = adapterMessages.getItem(arg2);
				// Toast.makeText(getActivity(), msg.message,
				// Toast.LENGTH_LONG).show();
				break;

			case 3:
				// ContactHistory history = adapterHistory.getItem(arg2);
				// Toast.makeText(getActivity(), history.number,
				// Toast.LENGTH_LONG).show();
				break;
		}

		return false;
	}

	protected void removeFromList(int position)
	{
		switch (tabPosition)
		{
			case 2:
				messages.remove(position);
				adapterMessages.notifyDataSetChanged();
				break;

			case 3:
				history.remove(position);
				adapterHistory.notifyDataSetChanged();
				break;
		}
	}

	@SuppressLint("SimpleDateFormat")
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3)
	{
		switch (tabPosition)
		{
			case 2:
				ContactMessage msg = adapterMessages.getItem(arg2);

				String message = "";
				if (msg.isFromContact())
					message = String.format(getString(R.string.contact_messages_received_at), VaultUtils.getDateFromLong(msg.getDate()));
				else
					message = String.format(getString(R.string.contact_messages_sent_at), VaultUtils.getDateFromLong(msg.getDate()), msg.getToNumber());

				Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
				break;
			case 3:
				ContactHistory history = adapterHistory.getItem(arg2);
				Toast.makeText(getActivity(), VaultUtils.getDateFromLong(history.getDate()), Toast.LENGTH_LONG).show();
				break;
		}
	}

	@Override
	public void onClick(DialogInterface dialog, int which)
	{
		if (which >= 0)
		{
			this.messagePhoneIndex = which;
		}
		else
		{
			if (this.messageDialogChecked)
			{
				this.vContact.optAskMessagePhone = false;
				this.vContact.optMessagePhoneIndex = this.messagePhoneIndex;
				this.cContact.save();
			}

			((EditText) getView().findViewById(R.id.sendTextMessage)).setText("");

			sendMessage(this.messageData, this.vContact, this.messagePhoneIndex);
		}
	}

	@Override
	public void onClick(View v)
	{
		messageDialogChecked = ((CheckBox) v).isChecked();
	}
}
