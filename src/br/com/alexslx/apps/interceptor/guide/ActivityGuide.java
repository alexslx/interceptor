/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.interceptor.guide;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import br.com.alexslx.apps.interceptor.ActivityAppSettings;
import br.com.alexslx.apps.interceptor.R;

import com.viewpagerindicator.CirclePageIndicator;

public class ActivityGuide extends Activity
{

	private ViewPager			guidePager;
	private GuidePagerAdapter	guideAdapter;
	private SharedPreferences	prefs;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.guide_warper);

		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		guidePager = (ViewPager) findViewById(R.id.guidePagerView);
		guideAdapter = new GuidePagerAdapter();
		guidePager.setAdapter(guideAdapter);

		CirclePageIndicator guideIndicator = (CirclePageIndicator) findViewById(R.id.guideIndicator);
		guideIndicator.setViewPager(guidePager);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.guide_warper, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
			case R.id.menu_skip:
				guidePager.setCurrentItem((guideAdapter.getCount() - 1));
				return true;

			case R.id.menu_settings:
				Intent intent = new Intent(this, ActivityAppSettings.class);
				startActivity(intent);
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onClickButton(View v)
	{
		switch (v.getId())
		{
			case R.id.guideFinishButton:
				prefs.edit().putBoolean("app_firstLogin", true).commit();
				finish();
				break;
		}
	}
}
