/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.interceptor.models;

public class CustomListSectionItem implements CustomListItem
{

	private String	entryTitle;

	public CustomListSectionItem(String title)
	{
		this.entryTitle = title;
	}

	@Override
	public boolean isSection()
	{
		return true;
	}

	public String getTitle()
	{
		return entryTitle;
	}

}
