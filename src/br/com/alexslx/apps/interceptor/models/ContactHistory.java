/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.interceptor.models;

import android.content.Context;
import android.content.Intent;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

public class ContactHistory extends SugarRecord<ContactHistory>
{

	private String				contactId;
	private long				date;
	private String				number;
	private int					type;

	@Ignore
	public static final int		TYPE_CALL_OUTGOING	= 0;
	@Ignore
	public static final int		TYPE_CALL_INCOMING	= 1;
	@Ignore
	public static final int		TYPE_CALL_MISSED	= 2;
	@Ignore
	public static final int		TYPE_SMS_OUTGOING	= 3;
	@Ignore
	public static final int		TYPE_SMS_INCOMING	= 4;
	@Ignore
	public static final String	BROADCAST_STRING	= "interceptor.history";

	public ContactHistory(Context context)
	{
		super(context);
	}

	public String getContactId()
	{
		return contactId;
	}

	public void setContactId(String contactId)
	{
		this.contactId = contactId;
	}

	public long getDate()
	{
		return date;
	}

	public void setDate(long date)
	{
		this.date = date;
	}

	public String getNumber()
	{
		return number;
	}

	public void setNumber(String number)
	{
		this.number = number;
	}

	public int getType()
	{
		return type;
	}

	public void setType(int type)
	{
		this.type = type;
	}

	public static ContactHistory newHistoryEntry(Context context, String contactId, int type, String callerNumber, boolean sendBroadcast)
	{
		ContactHistory data = new ContactHistory(context);
		data.setContactId(contactId);
		data.setDate(System.currentTimeMillis());
		data.setNumber(callerNumber);
		data.setType(ContactHistory.TYPE_CALL_MISSED);
		data.save();

		if (sendBroadcast)
		{
			Intent i = new Intent();
			i.setAction(BROADCAST_STRING + "." + contactId);
			i.putExtra(BROADCAST_STRING + ".id", data.getId());
			context.sendBroadcast(i);
		}

		return data;
	}
}
