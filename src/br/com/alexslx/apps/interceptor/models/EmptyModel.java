/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.interceptor.models;

import com.orm.SugarRecord;

import android.content.Context;

public class EmptyModel extends SugarRecord<EmptyModel>
{
	int	_placeholder;

	public EmptyModel(Context context)
	{
		super(context);
		_placeholder = 0;
	}
}
