/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.interceptor.models;

public interface CustomListItem
{
	public boolean isSection();
}
