/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.interceptor.models;

import android.content.Context;
import android.content.Intent;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

public class ContactMessage extends SugarRecord<ContactMessage>
{

	private String				contactId;
	private String				message;
	private String				toNumber;
	private long				date;
	private boolean				from_contact;

	@Ignore
	public static final String	BROADCAST_STRING	= "interceptor.message";

	public ContactMessage(Context context)
	{
		super(context);
	}

	public String getContactId()
	{
		return contactId;
	}

	public void setContactId(String contactId)
	{
		this.contactId = contactId;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public String getToNumber()
	{
		return toNumber;
	}

	public void setToNumber(String toNumber)
	{
		this.toNumber = toNumber;
	}

	public long getDate()
	{
		return date;
	}

	public void setDate(long date)
	{
		this.date = date;
	}

	public boolean isFromContact()
	{
		return from_contact;
	}

	public void setFromContact(boolean b)
	{
		this.from_contact = b;
	}

	public static ContactMessage newMessageEntry(Context context, String contactId, String message, boolean from_contact, boolean sendBroadcast)
	{
		ContactMessage msg = new ContactMessage(context);
		msg.setContactId(contactId);
		msg.setDate(System.currentTimeMillis());
		msg.setFromContact(from_contact);
		msg.setMessage(message);
		msg.save();
		
		if (sendBroadcast)
		{
			Intent i = new Intent();
			i.setAction(BROADCAST_STRING + "." + contactId);
			i.putExtra(BROADCAST_STRING + ".id", msg.getId());
			context.sendBroadcast(i);
		}

		return msg;
	}
}
