/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.interceptor.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import com.google.gson.Gson;

import android.annotation.SuppressLint;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Event;
import android.provider.ContactsContract.CommonDataKinds.GroupMembership;
import android.provider.ContactsContract.CommonDataKinds.Identity;
import android.provider.ContactsContract.CommonDataKinds.Im;
import android.provider.ContactsContract.CommonDataKinds.Nickname;
import android.provider.ContactsContract.CommonDataKinds.Note;
import android.provider.ContactsContract.CommonDataKinds.Organization;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.Relation;
import android.provider.ContactsContract.CommonDataKinds.SipAddress;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.CommonDataKinds.StructuredPostal;
import android.provider.ContactsContract.CommonDataKinds.Website;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.RawContacts;
import android.view.View;
import android.view.View.OnClickListener;
import br.com.alexslx.apps.interceptor.R;
import br.com.alexslx.apps.interceptor.utils.L;
import br.com.alexslx.apps.interceptor.utils.PhoneNumberUtils;

@SuppressLint("NewApi")
@SuppressWarnings("serial")
public class VaultContact implements Serializable
{

	private int									starred;
	private int									times_contacted;
	private int									last_time_contacted;
	private int									send_to_voicemail;

	private String								contact_id;
	private String								displayName;
	private String								custom_ringtone;

	// Options
	public boolean								optContactHide;
	public boolean								optMessagesHide;
	public boolean								optMessagesAlert;
	public boolean								optCallBlock;
	public boolean								optCallHide;
	public boolean								optCallAlert;

	public String								optMessageNotification;
	public String								optCallNotification;

	// View Options
	public boolean								optAskMessagePhone;
	public int									optMessagePhoneIndex;

	public ArrayList<HashMap<String, String>>	names;					// ContactsContract.CommonDataKinds.StructuredPostal
	public ArrayList<HashMap<String, String>>	aliases;				// ContactsContract.CommonDataKinds.Nickname
	public ArrayList<HashMap<String, String>>	phones;				// ContactsContract.CommonDataKinds.Phone
	public ArrayList<HashMap<String, String>>	events;				// ContactsContract.CommonDataKinds.Event
	public ArrayList<HashMap<String, String>>	emails;				// ContactsContract.CommonDataKinds.Email
	public ArrayList<HashMap<String, String>>	relations;				// ContactsContract.CommonDataKinds.Relation
	public ArrayList<HashMap<String, String>>	sipaddrs;				// ContactsContract.CommonDataKinds.SipAddress
	public ArrayList<HashMap<String, String>>	websites;				// ContactsContract.CommonDataKinds.Website
	public ArrayList<HashMap<String, String>>	postals;				// ContactsContract.CommonDataKinds.StructuredPostal
	public ArrayList<HashMap<String, String>>	notes;					// ContactsContract.CommonDataKinds.Note
	public ArrayList<HashMap<String, String>>	orgs;					// ContactsContract.CommonDataKinds.Organization
	public ArrayList<HashMap<String, String>>	ims;					// ContactsContract.CommonDataKinds.Im
	public ArrayList<HashMap<String, String>>	groups;				// ContactsContract.CommonDataKinds.GroupMembership
	public ArrayList<HashMap<String, String>>	identities;			// ContactsContract.CommonDataKinds.Identity

	public enum DetailType {
		NAME, PHONE, EMAIL, EVENT, RELATION, WEBSITE, ADDRESS, ALIAS,
		ORGANIZATION, MESSENGER, IDENTITY, SIPADDRESS, NOTE
	}

	public VaultContact()
	{
		names = new ArrayList<HashMap<String, String>>();
		aliases = new ArrayList<HashMap<String, String>>();
		phones = new ArrayList<HashMap<String, String>>();
		events = new ArrayList<HashMap<String, String>>();
		emails = new ArrayList<HashMap<String, String>>();
		relations = new ArrayList<HashMap<String, String>>();
		sipaddrs = new ArrayList<HashMap<String, String>>();
		websites = new ArrayList<HashMap<String, String>>();
		postals = new ArrayList<HashMap<String, String>>();
		notes = new ArrayList<HashMap<String, String>>();
		orgs = new ArrayList<HashMap<String, String>>();
		ims = new ArrayList<HashMap<String, String>>();
		groups = new ArrayList<HashMap<String, String>>();
		identities = new ArrayList<HashMap<String, String>>();

		starred = 0;
		times_contacted = 0;
		last_time_contacted = 0;
		send_to_voicemail = 0;

		contact_id = "";
		displayName = "";
		custom_ringtone = "";

		// Options
		optContactHide = false;
		optCallBlock = false;
		optMessagesHide = false;
		optMessagesAlert = false;
		optCallHide = false;
		optCallAlert = false;

		optMessageNotification = "";
		optCallNotification = "";

		// View Options
		optAskMessagePhone = true;
		optMessagePhoneIndex = 0;
	}

	public void setID(String i)
	{
		this.contact_id = i;
	}

	public String getID()
	{
		return this.contact_id;
	}

	public void setDisplayName(String name)
	{
		this.displayName = name;
	}

	public String getDisplayName()
	{
		return this.displayName;
	}

	public void setCustomRingtone(String url)
	{
		this.custom_ringtone = url;
	}

	public String getCustomRingtone()
	{
		return this.custom_ringtone;
	}

	public void setStarred(int i)
	{
		this.starred = i;
	}

	public int getStarred()
	{
		return this.starred;
	}

	public void setTimesContacted(int i)
	{
		this.times_contacted = i;
	}

	public int getTimesContacted()
	{
		return this.times_contacted;
	}

	public void setLastTimeContacted(int i)
	{
		this.last_time_contacted = i;
	}

	public int getLastTimeContacted()
	{
		return this.last_time_contacted;
	}

	public void setSendToVoicemail(int i)
	{
		this.send_to_voicemail = i;
	}

	public int getSendToVoicemail()
	{
		return this.send_to_voicemail;
	}

	public void addToList(ArrayList<HashMap<String, String>> list, String strArray[])
	{
		HashMap<String, String> map = new HashMap<String, String>();
		String dataNames[] = { ContactsContract.Data.DATA1, ContactsContract.Data.DATA2, ContactsContract.Data.DATA3, ContactsContract.Data.DATA4, ContactsContract.Data.DATA5, ContactsContract.Data.DATA6, ContactsContract.Data.DATA7, ContactsContract.Data.DATA8, ContactsContract.Data.DATA9, ContactsContract.Data.DATA10, ContactsContract.Data.DATA11, ContactsContract.Data.DATA12, ContactsContract.Data.DATA13, ContactsContract.Data.DATA14, ContactsContract.Data.DATA15 };

		for (int i = 0; i < dataNames.length; i++)
		{
			if (strArray[i] != null && strArray.length > 0)
			{
				map.put(dataNames[i], strArray[i]);
			}
		}

		list.add(map);
	}

	public boolean addDetail(DetailType type, String data[])
	{
		switch (type)
		{
			case NAME:
				addToList(names, data);
				break;
			case PHONE:
				addToList(phones, data);
				break;
			case EMAIL:
				addToList(emails, data);
				break;
			case EVENT:
				addToList(events, data);
				break;
			case RELATION:
				addToList(relations, data);
				break;
			case WEBSITE:
				addToList(websites, data);
				break;
			case ADDRESS:
				addToList(postals, data);
				break;
			case ALIAS:
				addToList(aliases, data);
				break;
			case ORGANIZATION:
				addToList(orgs, data);
				break;
			case MESSENGER:
				addToList(ims, data);
				break;
			case IDENTITY:
				addToList(identities, data);
				break;
			case SIPADDRESS:
				addToList(sipaddrs, data);
				break;
			case NOTE:
				addToList(notes, data);
				break;
			default:
				return false;
		}

		return true;
	}

	public void addGroup(String data1, String data2)
	{
		HashMap<String, String> map = new HashMap<String, String>();
		map.put(ContactsContract.Data.DATA1, data1);
		map.put(ContactsContract.CommonDataKinds.GroupMembership.GROUP_SOURCE_ID, data2);

		groups.add(map);
	}

	public String getInfos()
	{
		String text = "";

		if (optContactHide || optMessagesHide || optCallHide)
		{
			text += "Hide: ";
			if (optContactHide)
				text += "Contact, ";
			if (optMessagesHide)
				text += "Messages, ";
			if (optCallHide)
				text += "Calls, ";

			text = text.substring(0, text.length() - 2);
			text += " ";
		}

		if (optMessagesAlert || optCallAlert)
		{
			text += "Alert: ";
			if (optMessagesAlert)
				text += "Messages, ";
			if (optCallAlert)
				text += "Calls, ";

			text = text.substring(0, text.length() - 2);
		}

		return text;
	}

	public boolean createAndroidContact(Context context)
	{
		L.d("Creating a contact to add into android system");
		ArrayList<ContentProviderOperation> listOperations = new ArrayList<ContentProviderOperation>();
		int rawContactInsertIndex = listOperations.size();

		// IDK - Account Type?
		listOperations.add(ContentProviderOperation.newInsert(RawContacts.CONTENT_URI).withValue(RawContacts.ACCOUNT_TYPE, null).withValue(RawContacts.ACCOUNT_NAME, null).build());

		// Display Name
		listOperations.add(ContentProviderOperation.newInsert(Data.CONTENT_URI).withValueBackReference(Data.RAW_CONTACT_ID, rawContactInsertIndex).withValue(Data.MIMETYPE, StructuredName.CONTENT_ITEM_TYPE).withValue(StructuredName.DISPLAY_NAME, getDisplayName()).build());

		// Phones
		for (HashMap<String, String> data : phones)
		{
			listOperations.add(ContentProviderOperation.newInsert(Data.CONTENT_URI).withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex).withValue(Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE).withValue(Phone.NUMBER, data.get(ContactsContract.Data.DATA1)).withValue(Phone.TYPE, data.get(ContactsContract.Data.DATA2)).withValue(Phone.LABEL, data.get(ContactsContract.Data.DATA3)).build());
		}

		// Events
		for (HashMap<String, String> data : events)
		{
			listOperations.add(ContentProviderOperation.newInsert(Data.CONTENT_URI).withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex).withValue(Data.MIMETYPE, Event.CONTENT_ITEM_TYPE).withValue(ContactsContract.Data.DATA1, data.get(ContactsContract.Data.DATA1)).withValue(ContactsContract.Data.DATA2, data.get(ContactsContract.Data.DATA2)).withValue(ContactsContract.Data.DATA3, data.get(ContactsContract.Data.DATA3)).build());
		}

		// Email's
		for (HashMap<String, String> data : emails)
		{
			listOperations.add(ContentProviderOperation.newInsert(Data.CONTENT_URI).withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex).withValue(Data.MIMETYPE, Email.CONTENT_ITEM_TYPE).withValue(ContactsContract.Data.DATA1, data.get(ContactsContract.Data.DATA1)).withValue(ContactsContract.Data.DATA2, data.get(ContactsContract.Data.DATA2)).withValue(ContactsContract.Data.DATA3, data.get(ContactsContract.Data.DATA3)).build());
		}

		// Aliases (NickName)
		for (HashMap<String, String> data : aliases)
		{
			listOperations.add(ContentProviderOperation.newInsert(Data.CONTENT_URI).withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex).withValue(Data.MIMETYPE, Nickname.CONTENT_ITEM_TYPE).withValue(ContactsContract.Data.DATA1, data.get(ContactsContract.Data.DATA1)).withValue(ContactsContract.Data.DATA2, data.get(ContactsContract.Data.DATA2)).withValue(ContactsContract.Data.DATA3, data.get(ContactsContract.Data.DATA3)).build());
		}

		// Relations
		for (HashMap<String, String> data : relations)
		{
			listOperations.add(ContentProviderOperation.newInsert(Data.CONTENT_URI).withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex).withValue(Data.MIMETYPE, Relation.CONTENT_ITEM_TYPE).withValue(ContactsContract.Data.DATA1, data.get(ContactsContract.Data.DATA1)).withValue(ContactsContract.Data.DATA2, data.get(ContactsContract.Data.DATA2)).withValue(ContactsContract.Data.DATA3, data.get(ContactsContract.Data.DATA3)).build());
		}

		// SipAddress
		for (HashMap<String, String> data : sipaddrs)
		{
			listOperations.add(ContentProviderOperation.newInsert(Data.CONTENT_URI).withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex).withValue(Data.MIMETYPE, SipAddress.CONTENT_ITEM_TYPE).withValue(ContactsContract.Data.DATA1, data.get(ContactsContract.Data.DATA1)).withValue(ContactsContract.Data.DATA2, data.get(ContactsContract.Data.DATA2)).withValue(ContactsContract.Data.DATA3, data.get(ContactsContract.Data.DATA3)).build());
		}

		// WebSites
		for (HashMap<String, String> data : websites)
		{
			listOperations.add(ContentProviderOperation.newInsert(Data.CONTENT_URI).withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex).withValue(Data.MIMETYPE, Website.CONTENT_ITEM_TYPE).withValue(ContactsContract.Data.DATA1, data.get(ContactsContract.Data.DATA1)).withValue(ContactsContract.Data.DATA2, data.get(ContactsContract.Data.DATA2)).withValue(ContactsContract.Data.DATA3, data.get(ContactsContract.Data.DATA3)).build());
		}

		// Notes
		for (HashMap<String, String> data : notes)
		{
			listOperations.add(ContentProviderOperation.newInsert(Data.CONTENT_URI).withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex).withValue(Data.MIMETYPE, Note.CONTENT_ITEM_TYPE).withValue(ContactsContract.Data.DATA1, data.get(ContactsContract.Data.DATA1)).build());
		}

		// IM's
		for (HashMap<String, String> data : ims)
		{
			listOperations.add(ContentProviderOperation.newInsert(Data.CONTENT_URI).withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex).withValue(Data.MIMETYPE, Im.CONTENT_ITEM_TYPE).withValue(ContactsContract.Data.DATA1, data.get(ContactsContract.Data.DATA1)).withValue(ContactsContract.Data.DATA2, data.get(ContactsContract.Data.DATA2)).withValue(ContactsContract.Data.DATA3, data.get(ContactsContract.Data.DATA3)).withValue(ContactsContract.Data.DATA5, data.get(ContactsContract.Data.DATA5)).withValue(ContactsContract.Data.DATA6, data.get(ContactsContract.Data.DATA6)).build());
		}

		// Organizations
		for (HashMap<String, String> data : orgs)
		{
			listOperations.add(ContentProviderOperation.newInsert(Data.CONTENT_URI).withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex).withValue(Data.MIMETYPE, Organization.CONTENT_ITEM_TYPE).withValue(ContactsContract.Data.DATA1, data.get(ContactsContract.Data.DATA1)).withValue(ContactsContract.Data.DATA2, data.get(ContactsContract.Data.DATA2)).withValue(ContactsContract.Data.DATA3, data.get(ContactsContract.Data.DATA3)).withValue(ContactsContract.Data.DATA4, data.get(ContactsContract.Data.DATA4)).withValue(ContactsContract.Data.DATA5, data.get(ContactsContract.Data.DATA5)).withValue(ContactsContract.Data.DATA6, data.get(ContactsContract.Data.DATA6)).withValue(ContactsContract.Data.DATA7, data.get(ContactsContract.Data.DATA7)).withValue(ContactsContract.Data.DATA8, data.get(ContactsContract.Data.DATA8)).withValue(ContactsContract.Data.DATA9, data.get(ContactsContract.Data.DATA9)).withValue(ContactsContract.Data.DATA10, data.get(ContactsContract.Data.DATA10)).build());
		}

		// StructuredName
		for (HashMap<String, String> data : names)
		{
			listOperations.add(ContentProviderOperation.newInsert(Data.CONTENT_URI).withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex).withValue(Data.MIMETYPE, StructuredName.CONTENT_ITEM_TYPE).withValue(ContactsContract.Data.DATA1, data.get(ContactsContract.Data.DATA1)).withValue(ContactsContract.Data.DATA2, data.get(ContactsContract.Data.DATA2)).withValue(ContactsContract.Data.DATA3, data.get(ContactsContract.Data.DATA3)).withValue(ContactsContract.Data.DATA4, data.get(ContactsContract.Data.DATA4)).withValue(ContactsContract.Data.DATA5, data.get(ContactsContract.Data.DATA5)).withValue(ContactsContract.Data.DATA6, data.get(ContactsContract.Data.DATA6)).withValue(ContactsContract.Data.DATA7, data.get(ContactsContract.Data.DATA7)).withValue(ContactsContract.Data.DATA8, data.get(ContactsContract.Data.DATA8)).withValue(ContactsContract.Data.DATA9, data.get(ContactsContract.Data.DATA9)).build());
		}

		// StructuredPostal
		for (HashMap<String, String> data : postals)
		{
			listOperations.add(ContentProviderOperation.newInsert(Data.CONTENT_URI).withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex).withValue(Data.MIMETYPE, StructuredPostal.CONTENT_ITEM_TYPE).withValue(ContactsContract.Data.DATA1, data.get(ContactsContract.Data.DATA1)).withValue(ContactsContract.Data.DATA2, data.get(ContactsContract.Data.DATA2)).withValue(ContactsContract.Data.DATA3, data.get(ContactsContract.Data.DATA3)).withValue(ContactsContract.Data.DATA4, data.get(ContactsContract.Data.DATA4)).withValue(ContactsContract.Data.DATA5, data.get(ContactsContract.Data.DATA5)).withValue(ContactsContract.Data.DATA6, data.get(ContactsContract.Data.DATA6)).withValue(ContactsContract.Data.DATA7, data.get(ContactsContract.Data.DATA7)).withValue(ContactsContract.Data.DATA8, data.get(ContactsContract.Data.DATA8)).withValue(ContactsContract.Data.DATA9, data.get(ContactsContract.Data.DATA9)).withValue(ContactsContract.Data.DATA10, data.get(ContactsContract.Data.DATA10)).build());
		}

		// Groups
		for (HashMap<String, String> data : groups)
		{
			listOperations.add(ContentProviderOperation.newInsert(Data.CONTENT_URI).withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex).withValue(Data.MIMETYPE, GroupMembership.CONTENT_ITEM_TYPE).withValue(ContactsContract.Data.DATA1, data.get(ContactsContract.Data.DATA1)).build());
		}

		// Identities
		for (HashMap<String, String> data : identities)
		{
			listOperations.add(ContentProviderOperation.newInsert(Data.CONTENT_URI).withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex).withValue(Data.MIMETYPE, Identity.CONTENT_ITEM_TYPE).withValue(ContactsContract.Data.DATA1, data.get(ContactsContract.Data.DATA1)).withValue(ContactsContract.Data.DATA2, data.get(ContactsContract.Data.DATA2)).build());
		}

		try
		{
			L.d("Saving contact to system");
			ContentProviderResult[] result = context.getContentResolver().applyBatch(ContactsContract.AUTHORITY, listOperations);
			if (result[0] != null)
			{
				ContentValues updateValues = new ContentValues();
				updateValues.put(ContactsContract.Contacts.STARRED, getStarred());
				updateValues.put(ContactsContract.Contacts.SEND_TO_VOICEMAIL, getSendToVoicemail());
				updateValues.put(ContactsContract.Contacts.LAST_TIME_CONTACTED, getLastTimeContacted());
				updateValues.put(ContactsContract.Contacts.TIMES_CONTACTED, getTimesContacted());
				updateValues.put(ContactsContract.Contacts.CUSTOM_RINGTONE, getCustomRingtone());

				context.getContentResolver().update(result[0].uri, updateValues, null, null);
			}

			return true;
		}
		catch (RemoteException e)
		{
			return false;
		}
		catch (OperationApplicationException e)
		{
			return true;
		}
	}

	public Object clone()
	{
		try
		{
			return super.clone();
		}
		catch (CloneNotSupportedException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public String getNameType(Context c, int i, String custom)
	{
		if (i == 0)
			return custom;

		String types[] = c.getResources().getStringArray(R.array.namesTypes);

		return types[i];
	}

	public String getPhoneType(Context c, int i, String custom)
	{
		if (i == 0)
			return custom;

		return c.getResources().getString(ContactsContract.CommonDataKinds.Phone.getTypeLabelResource(i)); // types[i];
	}

	public String getEventType(Context c, int i, String custom)
	{
		if (i == 0)
			return custom;

		return c.getResources().getString(ContactsContract.CommonDataKinds.Event.getTypeResource(i)); // types[i];
	}

	public String getEmailType(Context c, int i, String custom)
	{
		if (i == 0)
			return custom;

		return c.getResources().getString(ContactsContract.CommonDataKinds.Email.getTypeLabelResource(i)); // types[i];
	}

	public String getRelationType(Context c, int i, String custom)
	{
		if (i == 0)
			return custom;

		return c.getResources().getString(ContactsContract.CommonDataKinds.Relation.getTypeLabelResource(i)); // types[i];
	}

	public String getSipType(Context c, int i, String custom)
	{
		if (i == 0)
			return custom;

		return c.getResources().getString(ContactsContract.CommonDataKinds.SipAddress.getTypeLabelResource(i)); // types[i];
	}

	public String getWebType(Context c, int i, String custom)
	{
		if (i == 0)
			return custom;

		String types[] = c.getResources().getStringArray(R.array.websiteTypes);

		return types[i];
	}

	public String getIMType(Context c, int i, String custom)
	{
		if (i == 0)
			return custom;

		return c.getResources().getString(ContactsContract.CommonDataKinds.Im.getTypeLabelResource(i)); // types[i];
	}

	public String getIMProtocol(Context c, int i, String custom)
	{
		if (i == -1)
			return custom;

		return c.getResources().getString(ContactsContract.CommonDataKinds.Im.getProtocolLabelResource(i)); // types[i];
	}

	public String getOrgType(Context c, int i, String custom)
	{
		if (i == 0)
			return custom;

		return c.getResources().getString(ContactsContract.CommonDataKinds.Organization.getTypeLabelResource(i)); // types[i];
	}

	public String getPostalType(Context c, int i, String custom)
	{
		if (i == 0)
			return custom;

		return c.getResources().getString(ContactsContract.CommonDataKinds.StructuredPostal.getTypeLabelResource(i)); // types[i];
	}

	public void fillListView(Context context, ArrayList<CustomListItem> infos)
	{
		// Phones
		if (phones.size() > 0)
		{
			infos.add(new CustomListSectionItem(context.getString(R.string.contact_details_phones)));

			for (final HashMap<String, String> data : phones)
			{
				CustomListEntryItem c = new CustomListEntryItem();
				c.addItem(R.id.customListText1, data.get(ContactsContract.Data.DATA1));
				c.addItem(R.id.customListText2, "");

				if (data.get(ContactsContract.Data.DATA2) != null)
					c.addItem(R.id.customListTitle, getPhoneType(context, Integer.parseInt(data.get(ContactsContract.Data.DATA2)), data.get(ContactsContract.Data.DATA3)));
				else
					c.addItem(R.id.customListTitle, "");

				c.setIcon(android.R.drawable.sym_action_call, new OnClickListener() {
					@Override
					public void onClick(View v)
					{
						Context ctx = v.getContext();
						SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);

						// FIXME: Please god, remove this hack and turn it to
						// sharp (#).
						Uri callUri = Uri.parse(String.format("tel:**%s*%s", prefs.getString("app_code", "8520"), data.get(ContactsContract.Data.DATA1)));
						L.d("Placing a call to " + callUri.toString());

						Intent intent = new Intent(Intent.ACTION_CALL);
						intent.setData(callUri);
						ctx.startActivity(intent);
					}

				});

				infos.add(c);
			}
		}

		// Email's
		if (emails.size() > 0)
		{
			infos.add(new CustomListSectionItem(context.getString(R.string.contact_details_emails)));

			for (HashMap<String, String> data : emails)
			{
				CustomListEntryItem c = new CustomListEntryItem();
				c.addItem(R.id.customListText1, data.get(ContactsContract.Data.DATA1));
				c.addItem(R.id.customListText2, "");

				if (data.get(ContactsContract.Data.DATA2) != null)
					c.addItem(R.id.customListTitle, getEmailType(context, Integer.parseInt(data.get(ContactsContract.Data.DATA2)), data.get(ContactsContract.Data.DATA3)));
				else
					c.addItem(R.id.customListTitle, "");

				infos.add(c);
			}
		}

		// Events
		if (events.size() > 0)
		{
			infos.add(new CustomListSectionItem(context.getString(R.string.contact_details_events)));

			for (HashMap<String, String> data : events)
			{
				CustomListEntryItem c = new CustomListEntryItem();
				c.addItem(R.id.customListText1, data.get(ContactsContract.Data.DATA1));
				c.addItem(R.id.customListText2, "");

				if (data.get(ContactsContract.Data.DATA2) != null)
					c.addItem(R.id.customListTitle, getEventType(context, Integer.parseInt(data.get(ContactsContract.Data.DATA2)), data.get(ContactsContract.Data.DATA3)));
				else
					c.addItem(R.id.customListTitle, "");

				infos.add(c);
			}
		}

		// WebSites
		if (websites.size() > 0)
		{
			infos.add(new CustomListSectionItem(context.getString(R.string.contact_details_websites)));

			for (HashMap<String, String> data : websites)
			{
				CustomListEntryItem c = new CustomListEntryItem();
				c.addItem(R.id.customListText1, data.get(ContactsContract.Data.DATA1));
				c.addItem(R.id.customListText2, "");

				if (data.get(ContactsContract.Data.DATA2) != null)
					c.addItem(R.id.customListTitle, getWebType(context, Integer.parseInt(data.get(ContactsContract.Data.DATA2)), data.get(ContactsContract.Data.DATA3)));
				else
					c.addItem(R.id.customListTitle, "");

				infos.add(c);
			}
		}

		// IM's
		if (ims.size() > 0)
		{
			infos.add(new CustomListSectionItem(context.getString(R.string.contact_details_messengers)));

			for (HashMap<String, String> data : ims)
			{
				CustomListEntryItem c = new CustomListEntryItem();
				c.addItem(R.id.customListText1, data.get(ContactsContract.Data.DATA1));

				if (data.get(ContactsContract.Data.DATA5) != null)
					c.addItem(R.id.customListText2, getIMProtocol(context, Integer.parseInt(data.get(ContactsContract.Data.DATA5)), data.get(ContactsContract.Data.DATA6)));
				else
					c.addItem(R.id.customListText2, "");

				if (data.get(ContactsContract.Data.DATA2) != null)
					c.addItem(R.id.customListTitle, getIMType(context, Integer.parseInt(data.get(ContactsContract.Data.DATA2)), data.get(ContactsContract.Data.DATA3)));
				else
					c.addItem(R.id.customListTitle, "");

				infos.add(c);
			}
		}

		// Organizations
		if (orgs.size() > 0)
		{
			infos.add(new CustomListSectionItem(context.getString(R.string.contact_details_organizations)));

			for (HashMap<String, String> data : orgs)
			{
				CustomListEntryItem c = new CustomListEntryItem();
				c.addItem(R.id.customListTitle, data.get(ContactsContract.Data.DATA1));
				c.addItem(R.id.customListText1, data.get(ContactsContract.Data.DATA4));

				if (data.get(ContactsContract.Data.DATA2) != null)
					c.addItem(R.id.customListText2, getOrgType(context, Integer.parseInt(data.get(ContactsContract.Data.DATA2)), data.get(ContactsContract.Data.DATA3)));
				else
					c.addItem(R.id.customListText2, "");

				infos.add(c);
			}
		}

		// StructuredPostal
		if (postals.size() > 0)
		{
			infos.add(new CustomListSectionItem(context.getString(R.string.contact_details_addresses)));

			for (final HashMap<String, String> data : postals)
			{
				CustomListEntryItem c = new CustomListEntryItem();
				c.addItem(R.id.customListText1, data.get(ContactsContract.Data.DATA1));
				c.addItem(R.id.customListText2, "");

				if (data.get(ContactsContract.Data.DATA2) != null)
					c.addItem(R.id.customListTitle, getPostalType(context, Integer.parseInt(data.get(ContactsContract.Data.DATA2)), data.get(ContactsContract.Data.DATA3)));
				else
					c.addItem(R.id.customListTitle, "");

				c.setIcon(R.drawable.ic_dialog_map, new OnClickListener() {
					@Override
					public void onClick(View v)
					{
						Intent intent = new Intent(Intent.ACTION_VIEW);
						intent.setData(Uri.parse("geo:0,0?q=" + data.get(ContactsContract.Data.DATA1)));
						v.getContext().startActivity(intent);
					}

				});

				infos.add(c);
			}
		}

		// Relations
		if (relations.size() > 0)
		{
			infos.add(new CustomListSectionItem(context.getString(R.string.contact_details_relations)));

			for (HashMap<String, String> data : relations)
			{
				CustomListEntryItem c = new CustomListEntryItem();
				c.addItem(R.id.customListText1, data.get(ContactsContract.Data.DATA1));
				c.addItem(R.id.customListText2, "");

				if (data.get(ContactsContract.Data.DATA2) != null)
					c.addItem(R.id.customListTitle, getRelationType(context, Integer.parseInt(data.get(ContactsContract.Data.DATA2)), data.get(ContactsContract.Data.DATA3)));
				else
					c.addItem(R.id.customListTitle, "");

				infos.add(c);
			}
		}

		// SipAddress
		if (sipaddrs.size() > 0)
		{
			infos.add(new CustomListSectionItem(context.getString(R.string.contact_details_sip)));

			for (HashMap<String, String> data : sipaddrs)
			{
				CustomListEntryItem c = new CustomListEntryItem();
				c.addItem(R.id.customListText1, data.get(ContactsContract.Data.DATA1));
				c.addItem(R.id.customListText2, "");

				if (data.get(ContactsContract.Data.DATA2) != null)
					c.addItem(R.id.customListTitle, getSipType(context, Integer.parseInt(data.get(ContactsContract.Data.DATA2)), data.get(ContactsContract.Data.DATA3)));
				else
					c.addItem(R.id.customListTitle, "");

				infos.add(c);
			}

		}

		// Notes
		if (notes.size() > 0)
		{
			infos.add(new CustomListSectionItem(context.getString(R.string.contact_details_notes)));

			for (HashMap<String, String> data : notes)
			{
				CustomListEntryItem c = new CustomListEntryItem();
				c.addItem(R.id.customListText1, data.get(ContactsContract.Data.DATA1));
				c.addItem(R.id.customListText2, "");
				c.addItem(R.id.customListTitle, "");
				infos.add(c);
			}
		}

		// Aliases (NickName)
		// StructuredName
		// Photo
		// Identity
		// GroupMembership
	}

	public String toString()
	{
		Gson gson = new Gson();
		return gson.toJson(this);
	}

	public boolean haveNumber(String callerNumber)
	{
		for (HashMap<String, String> data : phones)
		{
			if (PhoneNumberUtils.compareLoosely(callerNumber, data.get(ContactsContract.Data.DATA1)))
				return true;
		}
		return false;
	}
}
