/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.interceptor.models;

import java.io.Serializable;
import java.util.List;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import br.com.alexslx.apps.interceptor.R;

import com.google.gson.Gson;
import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

@SuppressWarnings("serial")
public class Contact extends SugarRecord<Contact> implements Serializable, Cloneable
{
	@Ignore
	private transient VaultContact	_contact	= null;

	private String					data;

	public Contact(Context context)
	{
		super(context);
		this.data = "";
	}

	public Contact(Context context, VaultContact c)
	{
		super(context);
		this._contact = c;
		this.data = "";
	}

	public VaultContact getContact()
	{
		if (this._contact == null)
			this._contact = new VaultContact();

		return this._contact;
	}

	public void load()
	{
		Gson gson = new Gson();
		this._contact = gson.fromJson(this.data, VaultContact.class);
	}

	@Override
	public void save()
	{
		Gson gson = new Gson();
		this.data = gson.toJson(this._contact);

		super.save();
	}

	@Override
	public Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}

	public String toString()
	{
		return this.data;
	}

	public static List<Contact> getAllContacts()
	{
		List<Contact> contacts = Contact.listAll(Contact.class);
		if (contacts != null)
		{
			for (Contact c : contacts)
			{
				c.load();
			}
		}

		return contacts;
	}

	public static Contact getContactByNumber(String number)
	{
		List<Contact> contacts = Contact.getAllContacts();
		if (contacts != null)
		{
			for (Contact c : contacts)
			{
				if (c.getContact().haveNumber(number))
				{
					return c;
				}
			}
		}
		return null;
	}

	public void sendNotification(Context context, boolean is_call)
	{
		// TODO: Please, rewrite this piece of code with some elegant way :)
		SharedPreferences mPrefs 					= PreferenceManager.getDefaultSharedPreferences(context);
		NotificationCompat.Builder mBuilder			= new NotificationCompat.Builder(context);
		NotificationManager mNotificationManager	= (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		
		// TODO: Should we allow to change alert icon and title?
		mBuilder.setSmallIcon(android.R.drawable.ic_dialog_alert);
		mBuilder.setContentTitle(context.getString(R.string.default_alert_title));
		mBuilder.setAutoCancel(true);
		mBuilder.setOnlyAlertOnce(true);
		//mBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);
		mBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS);
		
		String notifText = "";
		
		if (is_call)
		{
			if(this.getContact().optCallNotification.isEmpty())
			{
				notifText	= mPrefs.getString("app_alert_calls", context.getString(R.string.default_alert_calls) );
			}
			else
			{
				notifText	= this.getContact().optCallNotification;
			}
		}
		else
		{
			if(this.getContact().optMessageNotification.isEmpty())
			{
				notifText	= mPrefs.getString("app_alert_messages", context.getString(R.string.default_alert_messages) );
			}
			else
			{
				notifText	= this.getContact().optMessageNotification;
			}
		}
		
		mBuilder.setContentText(notifText);
		mBuilder.setTicker(notifText);
		
		mNotificationManager.notify(1, mBuilder.getNotification());
	}
}
