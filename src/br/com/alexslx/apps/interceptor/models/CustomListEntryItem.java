/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.interceptor.models;

import android.util.SparseArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class CustomListEntryItem implements CustomListItem
{

	private int					resourceId;
	private SparseArray<String>	items;

	private boolean				haveIcon;
	private OnClickListener		iconListener;
	private int					iconResource;

	public CustomListEntryItem(int resId)
	{
		items = new SparseArray<String>();
		this.resourceId = resId;
	}

	public CustomListEntryItem()
	{
		items = new SparseArray<String>();
		this.resourceId = 0;
		this.haveIcon = false;
		this.iconResource = 0;
	}

	public void addItem(int key, String value)
	{
		items.append(key, value);
	}

	public void setIcon(int res, OnClickListener listener)
	{
		this.iconResource = res;
		this.iconListener = listener;
		this.haveIcon = true;
	}

	public boolean withIcon()
	{
		return this.haveIcon;
	}

	public OnClickListener getIconListener()
	{
		return this.iconListener;
	}

	public void fillView(View v)
	{
		for (int i = 0; i < items.size(); i++)
		{
			int key = items.keyAt(i);
			String value = items.valueAt(i);

			TextView myView = (TextView) v.findViewById(key);
			if (myView != null)
				myView.setText(value);
		}
	}

	@Override
	public boolean isSection()
	{
		return false;
	}

	public int getResourceId()
	{
		return resourceId;
	}

	public int getIconResource()
	{
		return this.iconResource;
	}

}
