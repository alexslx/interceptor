/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.interceptor;

/*import android.app.TabActivity;
 import android.content.Intent;
 import android.os.Bundle;
 import android.widget.TabHost;

 @SuppressWarnings("deprecation")
 public class ContactDetails extends TabActivity {

 protected void onCreate(Bundle savedInstanceState)
 {
 super.onCreate(savedInstanceState);
 setContentView(R.layout.contact_detail);

 final TabHost tabHost = getTabHost();

 tabHost.addTab(tabHost.newTabSpec("ActivityContactOptions").setIndicator("Op��es").setContent(new Intent(this, ActivityContactOptions.class)));
 tabHost.addTab(tabHost.newTabSpec("ContactMessages").setIndicator("Mensagens").setContent(new Intent(this, ContactMessages.class)));
 tabHost.addTab(tabHost.newTabSpec("ContactHistory").setIndicator("Hist�rico").setContent(new Intent(this, ContactHistory.class)));
 }
 }
 */

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import br.com.alexslx.apps.interceptor.DetailsFragment.contactActivityInterface;
import br.com.alexslx.apps.interceptor.models.Contact;
import br.com.alexslx.apps.interceptor.models.ContactHistory;
import br.com.alexslx.apps.interceptor.models.ContactMessage;
import br.com.alexslx.apps.interceptor.models.VaultContact;
import br.com.alexslx.apps.interceptor.utils.L;

@SuppressLint("NewApi")
public class ActivityContactDetails extends FragmentActivity implements ActionBar.TabListener, OnClickListener, contactActivityInterface
{

	/**
	 * The serialization (saved instance state) Bundle key representing the
	 * current tab position.
	 */
	static final String					STATE_SELECTED_NAVIGATION_ITEM	= "selected_navigation_item";
	static final String					CURRENT_CONTACT					= "current_contact";

	static final int					RESULT_FROM_OPTIONS				= 1;

	public Contact						contact							= null;
	public VaultContact					vContact						= null;
	private ArrayList<ContactMessage>	messages;
	private ArrayList<ContactHistory>	history;

	private ProgressDialog				loader;

	private int							remove_type;

	private BroadcastReceiver			receiver;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contact_detail);

		this.contact = ((ApplicationHelper) getApplication()).getContact();
		this.vContact = this.contact.getContact();

		// Set up the action bar to show tabs.
		final ActionBar actionBar = getActionBar();
		actionBar.setTitle(this.contact.getContact().getDisplayName());
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.addTab(actionBar.newTab().setText(R.string.tab_title_infos).setTabListener(this));
		actionBar.addTab(actionBar.newTab().setText(R.string.tab_title_messages).setTabListener(this));
		actionBar.addTab(actionBar.newTab().setText(R.string.tab_title_history).setTabListener(this));
		actionBar.setIcon(R.drawable.ic_menu_friendslist);

		// Set up the broadcast event
		IntentFilter filter = new IntentFilter();
		filter.addAction(ContactHistory.BROADCAST_STRING + "." + this.vContact.getID());
		filter.addAction(ContactMessage.BROADCAST_STRING + "." + this.vContact.getID());
		receiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent)
			{
				String action 		= intent.getAction();
				if (action.contains(ContactMessage.BROADCAST_STRING))
				{
					L.d("Broadcast received from message receivers!");
					long contactId = intent.getLongExtra(ContactMessage.BROADCAST_STRING + ".id", 0);
					
					ContactMessage msg = ContactMessage.findById(ContactMessage.class, contactId);
					if(msg != null && messages != null)
					{
						L.d("Message added into list");
						messages.add(msg);
					}
				}
				else if (action.contains(ContactHistory.BROADCAST_STRING))
				{
					L.d("Broadcast received from call receivers!");
					long contactId = intent.getLongExtra(ContactHistory.BROADCAST_STRING + ".id", 0);
					
					ContactHistory data = ContactMessage.findById(ContactHistory.class, contactId);
					if(data != null && messages != null)
					{
						L.d("History added into list");
						history.add(data);
					}
				}
				else
				{
					L.d("Unknown action: " + action);
				}
			}
		};
		registerReceiver(receiver, filter);
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		if (receiver != null)
		{
			unregisterReceiver(receiver);
		}
	}

	@Override
	public void onBackPressed()
	{
		// super.onBackPressed();

		Intent i = new Intent(getApplicationContext(), ActivityContactsList.class);
		i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(i);
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState)
	{
		// Restore the previously serialized current tab position.
		if (savedInstanceState.containsKey(STATE_SELECTED_NAVIGATION_ITEM))
		{
			getActionBar().setSelectedNavigationItem(savedInstanceState.getInt(STATE_SELECTED_NAVIGATION_ITEM));
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		// Serialize the current tab position.
		outState.putInt(STATE_SELECTED_NAVIGATION_ITEM, getActionBar().getSelectedNavigationIndex());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.contact_detail, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
			case android.R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				return true;

			case R.id.menu_contact_options:
				((ApplicationHelper) getApplication()).setContact(this.contact);
				Intent i = new Intent(this, ActivityContactOptions.class);
				startActivityForResult(i, RESULT_FROM_OPTIONS);
				return true;

			case R.id.menu_contact_del:
				removeContactAlert();
				return true;

			case R.id.menu_test:

				return true;

			case R.id.menu_settings:
				Intent intent = new Intent(this, ActivityAppSettings.class);
				startActivity(intent);
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (resultCode == RESULT_OK)
		{
			if (requestCode == RESULT_FROM_OPTIONS)
			{
				this.contact = ((ApplicationHelper) getApplication()).getContact();
			}
		}
	}

	private void removeContactAlert()
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(this.contact.getContact().getDisplayName());
		builder.setSingleChoiceItems(R.array.contact_remove_options, 0, this);
		builder.setPositiveButton(getString(R.string.general_cancel), null);
		builder.setNegativeButton(getString(R.string.general_exclude), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				Intent i = new Intent(getApplicationContext(), ActivityContactsList.class);
				Bundle extras = new Bundle();
				extras.putInt("action", ActivityContactsList.ACTION_REMOVE);
				extras.putString("contact_id", contact.getContact().getID());
				extras.putInt("action_type", remove_type);

				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
				i.putExtras(extras);
				startActivity(i);
			}

		});

		builder.show();
	}

	public ArrayList<ContactMessage> loadMessages()
	{
		if (messages == null)
		{
			messages = new ArrayList<ContactMessage>();

			loader = ProgressDialog.show(this, getString(R.string.loading), getString(R.string.loading_contact_messages), true);
			new LoadMessagesTask().execute("");
		}

		return messages;
	}

	public ArrayList<ContactHistory> loadHistory()
	{
		if (history == null)
		{
			history = new ArrayList<ContactHistory>();

			loader = ProgressDialog.show(this, getString(R.string.loading), getString(R.string.loading_contact_history), true);
			new LoadHistoryTask().execute("");
		}

		return history;
	}

	private class LoadMessagesTask extends AsyncTask<String, Void, String>
	{
		protected String doInBackground(String... params)
		{
			L.d("Starting ASyncTask to load contact messages");

			List<ContactMessage> objects = null;

			try
			{
				String contact_id = ((ApplicationHelper) getApplication()).getContact().getContact().getID();
				String whereClauses[] = new String[1];
				whereClauses[0] = contact_id;

				objects = ContactMessage.find(ContactMessage.class, "contact_id = ?", whereClauses);
			}
			catch (RuntimeException e)
			{
				L.d("RuntimeException with listAll: " + e.getMessage());
			}

			if (objects != null)
			{
				for (ContactMessage m : objects)
				{
					messages.add(m);
				}
			}

			if (ActivityContactDetails.this.loader != null)
			{
				L.d("Hiding loader from caller activity");
				ActivityContactDetails.this.loader.dismiss();
			}
			return null;
		}
	}

	private class LoadHistoryTask extends AsyncTask<String, Void, String>
	{
		protected String doInBackground(String... params)
		{
			L.d("Starting ASyncTask to load contact history");

			List<ContactHistory> objects = null;

			try
			{
				String contact_id = ((ApplicationHelper) getApplication()).getContact().getContact().getID();
				String whereClauses[] = new String[1];
				whereClauses[0] = contact_id;

				objects = ContactHistory.find(ContactHistory.class, "contact_id = ?", whereClauses);
			}
			catch (RuntimeException e)
			{
				L.d("RuntimeException with listAll: " + e.getMessage());
			}

			if (objects != null)
			{
				for (ContactHistory h : objects)
				{
					history.add(h);
				}
			}

			if (ActivityContactDetails.this.loader != null)
			{
				L.d("Hiding loader from caller activity");
				ActivityContactDetails.this.loader.dismiss();
			}
			return null;
		}
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
	{
		Fragment fragment = new DetailsFragment();
		Bundle args = new Bundle();
		int tabPosition = tab.getPosition() + 1;

		((ApplicationHelper) getApplication()).setContact(this.contact);

		args.putInt(DetailsFragment.ARG_SECTION_NUMBER, tabPosition);
		fragment.setArguments(args);

		getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
	{
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
	{
	}

	@Override
	public void onClick(DialogInterface dialog, int which)
	{
		this.remove_type = which;
	}

}
