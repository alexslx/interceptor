/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.interceptor;

import br.com.alexslx.apps.interceptor.models.Contact;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class ActivityContactOptions extends Activity implements OnClickListener
{

	public Contact	contact			= null;
	public Contact	contact_backup	= null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contact_options);

		this.contact = ((ApplicationHelper) getApplication()).getContact();
		this.contact_backup = (Contact) this.contact.getContact().clone();

		((CheckBox) findViewById(R.id.optContactHide)).setOnClickListener(this);
		((CheckBox) findViewById(R.id.optCallBlock)).setOnClickListener(this);
		((CheckBox) findViewById(R.id.optMessagesHide)).setOnClickListener(this);
		((CheckBox) findViewById(R.id.optMessagesAlert)).setOnClickListener(this);
		((CheckBox) findViewById(R.id.optCallHide)).setOnClickListener(this);
		((CheckBox) findViewById(R.id.optCallAlert)).setOnClickListener(this);

		loadContact();
	}

	private void loadContact()
	{
		((CheckBox) findViewById(R.id.optContactHide)).setChecked(contact.getContact().optContactHide);
		((CheckBox) findViewById(R.id.optCallBlock)).setChecked(contact.getContact().optCallBlock);
		((CheckBox) findViewById(R.id.optMessagesHide)).setChecked(contact.getContact().optMessagesHide);
		((CheckBox) findViewById(R.id.optMessagesAlert)).setChecked(contact.getContact().optMessagesAlert);
		((CheckBox) findViewById(R.id.optCallHide)).setChecked(contact.getContact().optCallHide);
		((CheckBox) findViewById(R.id.optCallAlert)).setChecked(contact.getContact().optCallAlert);

		((EditText) findViewById(R.id.optMessageNotification)).setText(contact.getContact().optMessageNotification);
		((EditText) findViewById(R.id.optCallNotification)).setText(contact.getContact().optCallNotification);
	}

	@Override
	public void onClick(View v)
	{
		finishSave();
	}

	public void finishSave()
	{
		this.contact.getContact().optContactHide = ((CheckBox) findViewById(R.id.optContactHide)).isChecked();
		this.contact.getContact().optCallBlock = ((CheckBox) findViewById(R.id.optCallBlock)).isChecked();
		this.contact.getContact().optMessagesHide = ((CheckBox) findViewById(R.id.optMessagesHide)).isChecked();
		this.contact.getContact().optMessagesAlert = ((CheckBox) findViewById(R.id.optMessagesAlert)).isChecked();
		this.contact.getContact().optCallHide = ((CheckBox) findViewById(R.id.optCallHide)).isChecked();
		this.contact.getContact().optCallAlert = ((CheckBox) findViewById(R.id.optCallAlert)).isChecked();

		contact.getContact().optMessageNotification = ((EditText) findViewById(R.id.optMessageNotification)).getText().toString();
		contact.getContact().optCallNotification = ((EditText) findViewById(R.id.optCallNotification)).getText().toString();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.contact_options, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{

			case R.id.menu_contact_options_save:
				Toast.makeText(this, getString(R.string.contact_options_saved), Toast.LENGTH_LONG).show();

				finishSave();
				this.contact.save();
				this.setResult(RESULT_OK, getIntent());
				this.finish();
				return true;
			case R.id.menu_contact_options_cancel:
				Toast.makeText(this, getString(R.string.contact_options_cancelled), Toast.LENGTH_LONG).show();

				this.contact = this.contact_backup;
				this.setResult(RESULT_CANCELED, getIntent());
				this.finish();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
