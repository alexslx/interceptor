/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.interceptor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import br.com.alexslx.apps.interceptor.adapters.AdapterVaultListView;
import br.com.alexslx.apps.interceptor.guide.ActivityGuide;
import br.com.alexslx.apps.interceptor.models.Contact;
import br.com.alexslx.apps.interceptor.models.VaultContact;
import br.com.alexslx.apps.interceptor.models.VaultContact.DetailType;
import br.com.alexslx.apps.interceptor.utils.L;
import br.com.alexslx.apps.interceptor.utils.VaultUtils;

public class ActivityContactsList extends Activity implements OnItemClickListener, OnItemLongClickListener
{

	private static final int		CONTACT_PICKER_RESULT	= 13371;
	public static final int			ACTION_NONE				= 0;
	public static final int			ACTION_REMOVE			= 1;
	public static final int			ACTION_UPDATE			= 2;

	private ListView				listView;
	private AdapterVaultListView	adapterListView;
	private ArrayList<Contact>		contacts;
	private ProgressDialog			loader;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contacts_list);

		// Show the Up button in the action bar.
		getActionBar().setDisplayHomeAsUpEnabled(false);

		// Takes references and set listener
		listView = (ListView) findViewById(R.id.contactList);
		listView.setOnItemClickListener(this);
		listView.setOnItemLongClickListener(this);
		listView.setLongClickable(true);

		loadVaultList();
	}

	@Override
	public void onResume()
	{
		super.onResume();

		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		if (sharedPrefs.getBoolean("app_firstLogin", false) == false)
		{
			Intent i = new Intent(this, ActivityGuide.class);
			startActivity(i);
		}
	}

	private void loadVaultList()
	{
		contacts = ((ApplicationHelper) getApplication()).getContactList();
		contacts.clear();

		loader = ProgressDialog.show(this, getString(R.string.loading), getString(R.string.loading_contacts), true);
		// new LoadVaultTask().execute("");
		loadVault();

		adapterListView = new AdapterVaultListView(this, contacts);
		listView.setAdapter(adapterListView);
		listView.setCacheColorHint(Color.TRANSPARENT);
		adapterListView.notifyDataSetChanged();
	}

	private void loadVault()
	{
		L.d("loadVault() Initialized");
		Bundle extras = getIntent().getExtras();
		int actionId = ACTION_NONE;
		String contactId = "";
		if (extras != null)
		{
			actionId = extras.getInt("action", ACTION_NONE);
			contactId = extras.getString("contact_id");
		}

		List<Contact> objects = Contact.listAll(Contact.class);
		for (Contact c : objects)
		{
			c.load();
			if (c.getContact().getID().equalsIgnoreCase(contactId))
			{
				switch (actionId)
				{
					case ACTION_REMOVE:
						L.d("Removing contact from database");
						int actionType = extras.getInt("action_type", 0);
						if (actionType == 1)
							c.getContact().createAndroidContact(getApplicationContext());

						c.delete();
						continue;

					case ACTION_UPDATE:
						L.d("Updating contact");
						c.save();
						break;
				}
			}
			contacts.add(c);
		}

		Collections.sort(contacts, new VaultUtils());

		if (this.loader != null)
		{
			L.d("Hiding loader from caller activity");
			this.loader.dismiss();
		}

		if (this.adapterListView != null)
		{
			L.d("Refreshing ListView adapter");
			this.adapterListView.notifyDataSetChanged();
		}

		L.d("loadVault() finished");
	}

	private class LoadVaultTask extends AsyncTask<String, Void, String>
	{
		@Override
		protected void onPostExecute(final String data)
		{
			if (ActivityContactsList.this.loader != null)
			{
				L.d("Hiding loader from caller activity");
				ActivityContactsList.this.loader.dismiss();
			}

			if (ActivityContactsList.this.adapterListView != null)
			{
				L.d("Refreshing ListView adapter");
				ActivityContactsList.this.adapterListView.notifyDataSetChanged();
			}
		}

		protected String doInBackground(String... params)
		{
			L.d("Started ASyncTask to load contacts from database");

			Bundle extras = getIntent().getExtras();
			int actionId = ACTION_NONE;
			String contactId = "";
			if (extras != null)
			{
				actionId = extras.getInt("action", ACTION_NONE);
				contactId = extras.getString("contact_id");
			}

			List<Contact> objects = Contact.listAll(Contact.class);
			for (Contact c : objects)
			{
				c.load();
				if (c.getContact().getID().equalsIgnoreCase(contactId))
				{
					switch (actionId)
					{
						case ACTION_REMOVE:
							L.d("Removing contact from database");
							int actionType = extras.getInt("action_type", 0);
							if (actionType == 1)
								c.getContact().createAndroidContact(getApplicationContext());

							c.delete();
							continue;

						case ACTION_UPDATE:
							L.d("Updating contact");
							c.save();
							break;
					}
				}
				contacts.add(c);
			}

			L.d("ASyncTask finished");
			Collections.sort(contacts, new VaultUtils());
			return null;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.contacts_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		Intent intent = null;
		switch (item.getItemId())
		{
			case R.id.menu_settings:
				intent = new Intent(this, ActivityAppSettings.class);
				startActivity(intent);
				return true;

			case R.id.menu_test:
				onTestMenu();
				return true;

			case R.id.menu_contact_import:
				intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
				startActivityForResult(intent, CONTACT_PICKER_RESULT);
				return true;

			case R.id.menu_contact_add:
				// intent = new Intent(this, ActivityContactAdd.class);
				// startActivity(intent);
				return true;

			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void onTestMenu()
	{
		Intent intent = new Intent(this, ActivityGuide.class);
		startActivity(intent);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent)
	{
		super.onActivityResult(requestCode, resultCode, intent);
		if (requestCode == CONTACT_PICKER_RESULT && resultCode == Activity.RESULT_OK)
		{
			loader = ProgressDialog.show(this, getString(R.string.loading), getString(R.string.loading_contact_info), true);
			new LoadContactTask().execute(intent.getData());
		}
	}

	private void finishLoadContactSuccess(Contact c)
	{
		if (c == null)
			return;

		c.save();
		contacts.add(c);
		Collections.sort(contacts, new VaultUtils());
		adapterListView.notifyDataSetChanged();
	}

	private void finishLoadContactError(int type)
	{
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
		alertDialog.setTitle(getString(R.string.contact_addlist_error_title));
		alertDialog.setIcon(android.R.drawable.ic_dialog_info);
		switch (type)
		{
			case 1:
				alertDialog.setMessage(getString(R.string.contact_addlist_error_already));
				break;
			case 2:
				alertDialog.setMessage(getString(R.string.contact_addlist_error_nophone));
				break;
			default:
				alertDialog.setMessage(getString(R.string.contact_addlist_error_generic));
				break;
		}
		alertDialog.setPositiveButton(getString(android.R.string.ok), null);
		alertDialog.show();
	}

	private void addDetailToContact(Cursor dataCursor, VaultContact c, DetailType type, int size)
	{
		String[] dataArray = new String[15];
		String dataNames[] = { ContactsContract.Data.DATA1, ContactsContract.Data.DATA2, ContactsContract.Data.DATA3, ContactsContract.Data.DATA4, ContactsContract.Data.DATA5, ContactsContract.Data.DATA6, ContactsContract.Data.DATA7, ContactsContract.Data.DATA8, ContactsContract.Data.DATA9, ContactsContract.Data.DATA10, ContactsContract.Data.DATA11, ContactsContract.Data.DATA12, ContactsContract.Data.DATA13, ContactsContract.Data.DATA14, ContactsContract.Data.DATA15 };

		for (int i = 0; i < size; i++)
		{
			String data = "";
			try
			{
				data = dataCursor.getString(dataCursor.getColumnIndex(dataNames[i]));
			}
			catch (Exception e)
			{
				// Just fill blank, ops, already filled
			}
			dataArray[i] = data;
		}

		c.addDetail(type, dataArray);
	}

	private final Cursor getDetail(String contactId, String mimeType)
	{
		String where = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
		String[] whereParameters = new String[] { contactId, mimeType };
		return getContentResolver().query(ContactsContract.Data.CONTENT_URI, null, where, whereParameters, null);
	}

	private class LoadContactResult
	{
		public int		error	= 0;
		public Contact	c		= null;

		public LoadContactResult(int e)
		{
			this.error = e;
		}

		public LoadContactResult(Contact c)
		{
			this.c = c;
		}
	}

	private class LoadContactTask extends AsyncTask<Uri, Void, LoadContactResult>
	{
		@Override
		protected void onPostExecute(final LoadContactResult result)
		{
			if (ActivityContactsList.this.loader != null)
			{
				L.d("Hiding loader from caller activity");
				ActivityContactsList.this.loader.dismiss();
			}

			if (result.error != 0 || result.c == null)
			{
				ActivityContactsList.this.finishLoadContactError(result.error);
			}
			else
			{
				ActivityContactsList.this.finishLoadContactSuccess(result.c);
			}
		}

		protected LoadContactResult doInBackground(Uri... params)
		{
			L.d("Starting ASyncTask to load contact from system");

			Cursor cursor = managedQuery(params[0], null, null, null, null);
			Contact c = null;

			while (cursor.moveToNext())
			{
				String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
				String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
				String hasPhone = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
				Cursor dataCursor = null;

				if (hasPhone.equalsIgnoreCase("0"))
				{
					L.d("Contact don't have a phone.");
					return new LoadContactResult(1);
				}

				L.d("Contact ID is " + contactId);

				if (checkContactListById(contactId))
				{
					L.d("Contact is already on our list");
					return new LoadContactResult(2);
				}

				c = new Contact(ActivityContactsList.this);
				c.getContact().setID(contactId);
				c.getContact().setDisplayName(contactName);

				c.getContact().setStarred(cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.STARRED)));
				c.getContact().setTimesContacted(cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.TIMES_CONTACTED)));
				c.getContact().setLastTimeContacted(cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.LAST_TIME_CONTACTED)));
				c.getContact().setSendToVoicemail(cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.SEND_TO_VOICEMAIL)));
				c.getContact().setCustomRingtone(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.CUSTOM_RINGTONE)));

				// Phones
				dataCursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
				while (dataCursor.moveToNext())
				{
					L.d("Found contact phone");
					addDetailToContact(dataCursor, c.getContact(), VaultContact.DetailType.PHONE, 3);
				}
				dataCursor.close();

				// Events
				dataCursor = getDetail(contactId, ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE);
				while (dataCursor.moveToNext())
				{
					L.d("Found contact event");
					addDetailToContact(dataCursor, c.getContact(), VaultContact.DetailType.EVENT, 3);
				}
				dataCursor.close();

				// Email's
				dataCursor = getDetail(contactId, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE);
				while (dataCursor.moveToNext())
				{
					L.d("Found contact email");
					addDetailToContact(dataCursor, c.getContact(), VaultContact.DetailType.EMAIL, 3);
				}
				dataCursor.close();

				// Aliases (NickName)
				dataCursor = getDetail(contactId, ContactsContract.CommonDataKinds.Nickname.CONTENT_ITEM_TYPE);
				while (dataCursor.moveToNext())
				{
					L.d("Found contact alias");
					addDetailToContact(dataCursor, c.getContact(), VaultContact.DetailType.ALIAS, 3);
				}
				dataCursor.close();

				// Relations
				dataCursor = getDetail(contactId, ContactsContract.CommonDataKinds.Relation.CONTENT_ITEM_TYPE);
				while (dataCursor.moveToNext())
				{
					L.d("Found contact relation");
					addDetailToContact(dataCursor, c.getContact(), VaultContact.DetailType.RELATION, 3);
				}
				dataCursor.close();

				// SipAddress
				dataCursor = getDetail(contactId, ContactsContract.CommonDataKinds.SipAddress.CONTENT_ITEM_TYPE);
				while (dataCursor.moveToNext())
				{
					L.d("Found contact SIP address");
					addDetailToContact(dataCursor, c.getContact(), VaultContact.DetailType.SIPADDRESS, 3);
				}
				dataCursor.close();

				// Website's
				dataCursor = getDetail(contactId, ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE);
				while (dataCursor.moveToNext())
				{
					L.d("Found contact website");
					addDetailToContact(dataCursor, c.getContact(), VaultContact.DetailType.WEBSITE, 3);
				}
				dataCursor.close();

				// IM's
				dataCursor = getDetail(contactId, ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE);
				while (dataCursor.moveToNext())
				{
					L.d("Found contact IM address");
					addDetailToContact(dataCursor, c.getContact(), VaultContact.DetailType.MESSENGER, 6);
				}
				dataCursor.close();

				// Notes
				dataCursor = getDetail(contactId, ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE);
				while (dataCursor.moveToNext())
				{
					L.d("Found contact note");
					addDetailToContact(dataCursor, c.getContact(), VaultContact.DetailType.NOTE, 1);
				}
				dataCursor.close();

				// Organizations
				dataCursor = getDetail(contactId, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE);
				while (dataCursor.moveToNext())
				{
					L.d("Found contact organization");
					addDetailToContact(dataCursor, c.getContact(), VaultContact.DetailType.ORGANIZATION, 10);
				}
				dataCursor.close();

				// StructuredName
				dataCursor = getDetail(contactId, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE);
				while (dataCursor.moveToNext())
				{
					L.d("Found contact name detail");
					addDetailToContact(dataCursor, c.getContact(), VaultContact.DetailType.NAME, 10);
				}
				dataCursor.close();

				// StructuredPostal
				dataCursor = getDetail(contactId, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE);
				while (dataCursor.moveToNext())
				{
					L.d("Found contact address");
					addDetailToContact(dataCursor, c.getContact(), VaultContact.DetailType.ADDRESS, 10);
				}
				dataCursor.close();

				// Identities
				dataCursor = getDetail(contactId, ContactsContract.CommonDataKinds.Identity.CONTENT_ITEM_TYPE);
				while (dataCursor.moveToNext())
				{
					L.d("Found contact identity");
					addDetailToContact(dataCursor, c.getContact(), VaultContact.DetailType.IDENTITY, 2);
				}
				dataCursor.close();

				// Groups
				dataCursor = getDetail(contactId, ContactsContract.CommonDataKinds.GroupMembership.CONTENT_ITEM_TYPE);
				while (dataCursor.moveToNext())
				{
					L.d("Found contact group");
					String data1 = dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.Data.DATA1));
					String data2 = dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.CommonDataKinds.GroupMembership.GROUP_SOURCE_ID));

					c.getContact().addGroup(data1, data2);
				}
				dataCursor.close();

				L.d("Finished load contact informations");
			}

			return new LoadContactResult(c);
		}
	}

	private boolean checkContactListById(String contactId)
	{
		for (int i = 0; i < contacts.size(); i++)
		{
			if (contacts.get(i).getContact().getID().equalsIgnoreCase(contactId))
				return true;
		}
		return false;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int index, long arg3)
	{
		Contact c = adapterListView.getItem(index);

		((ApplicationHelper) getApplication()).setContact(c);

		Intent i = new Intent(this, ActivityContactDetails.class);
		startActivity(i);
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int index, long arg3)
	{
		Contact c = adapterListView.getItem(index);

		((ApplicationHelper) getApplication()).setContact(c);
		
		Intent i = new Intent(this, ActivityContactOptions.class);
		startActivity(i);
		return true;
	}
}
