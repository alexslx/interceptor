/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.interceptor.utils;

import android.util.Log;

public class L
{

	public static void d(final String msg)
	{
		final Throwable t = new Throwable();
		final StackTraceElement[] elements = t.getStackTrace();
		final String callerClassName = elements[1].getClassName();
		final String callerMethodName = elements[1].getMethodName();

		Log.d(callerClassName, String.format("[%s] %s", callerMethodName, msg));
	}

	public static void e(final String msg)
	{
		final Throwable t = new Throwable();
		final StackTraceElement[] elements = t.getStackTrace();
		final String callerClassName = elements[1].getClassName();
		final String callerMethodName = elements[1].getMethodName();

		Log.e(callerClassName, String.format("[%s] %s", callerMethodName, msg));
	}

	public static void i(final String msg)
	{
		final Throwable t = new Throwable();
		final StackTraceElement[] elements = t.getStackTrace();
		final String callerClassName = elements[1].getClassName();
		final String callerMethodName = elements[1].getMethodName();

		Log.i(callerClassName, String.format("[%s] %s", callerMethodName, msg));
	}

	public static void v(final String msg)
	{
		final Throwable t = new Throwable();
		final StackTraceElement[] elements = t.getStackTrace();
		final String callerClassName = elements[1].getClassName();
		final String callerMethodName = elements[1].getMethodName();

		Log.v(callerClassName, String.format("[%s] %s", callerMethodName, msg));
	}

	public static void w(final String msg)
	{
		final Throwable t = new Throwable();
		final StackTraceElement[] elements = t.getStackTrace();
		final String callerClassName = elements[1].getClassName();
		final String callerMethodName = elements[1].getMethodName();

		Log.w(callerClassName, String.format("[%s] %s", callerMethodName, msg));
	}

	public static void wtf(final String msg)
	{
		final Throwable t = new Throwable();
		final StackTraceElement[] elements = t.getStackTrace();
		final String callerClassName = elements[1].getClassName();
		final String callerMethodName = elements[1].getMethodName();

		Log.wtf(callerClassName, String.format("[%s] %s", callerMethodName, msg));
	}
}
