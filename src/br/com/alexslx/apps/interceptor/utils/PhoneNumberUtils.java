/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package br.com.alexslx.apps.interceptor.utils;

import android.util.SparseIntArray;

/**
 * Various utilities for dealing with phone number strings.
 */
public class PhoneNumberUtils
{
	/*
	 * Special characters
	 * 
	 * (See "What is a phone number?" doc) 'p' --- GSM pause character, same as
	 * comma 'n' --- GSM wild character 'w' --- GSM wait character
	 */
	public static final char	PAUSE		= ',';
	public static final char	WAIT		= ';';
	public static final char	WILD		= 'N';

	// Three and four digit phone numbers for either special services,
	// or 3-6 digit addresses from the network (eg carrier-originated SMS
	// messages) should
	// not match.
	//
	// This constant used to be 5, but SMS short codes has increased in length
	// and
	// can be easily 6 digits now days. Most countries have SMS short code
	// length between
	// 3 to 6 digits. The exceptions are
	//
	// Australia: Short codes are six or eight digits in length, starting with
	// the prefix "19"
	// followed by an additional four or six digits and two.
	// Czech Republic: Codes are seven digits in length for MO and five (not
	// billed) or
	// eight (billed) for MT direction
	//
	// see http://en.wikipedia.org/wiki/Short_code#Regional_differences for
	// reference
	//
	// However, in order to loose match 650-555-1212 and 555-1212, we need to
	// set the min match
	// to 7.
	static final int			MIN_MATCH	= 7;

	/** True if c is ISO-LATIN characters 0-9 */
	public static boolean isISODigit(char c)
	{
		return c >= '0' && c <= '9';
	}

	/** True if c is ISO-LATIN characters 0-9, *, # */
	public final static boolean is12Key(char c)
	{
		return (c >= '0' && c <= '9') || c == '*' || c == '#';
	}

	/** True if c is ISO-LATIN characters 0-9, *, # , +, WILD */
	public final static boolean isDialable(char c)
	{
		return (c >= '0' && c <= '9') || c == '*' || c == '#' || c == '+' || c == WILD;
	}

	/** True if c is ISO-LATIN characters 0-9, *, # , + (no WILD) */
	public final static boolean isReallyDialable(char c)
	{
		return (c >= '0' && c <= '9') || c == '*' || c == '#' || c == '+';
	}

	/** True if c is ISO-LATIN characters 0-9, *, # , +, WILD, WAIT, PAUSE */
	public final static boolean isNonSeparator(char c)
	{
		return (c >= '0' && c <= '9') || c == '*' || c == '#' || c == '+' || c == WILD || c == WAIT || c == PAUSE;
	}

	/**
	 * This any anything to the right of this char is part of the post-dial
	 * string (eg this is PAUSE or WAIT)
	 */
	public final static boolean isStartsPostDial(char c)
	{
		return c == PAUSE || c == WAIT;
	}

	private static boolean isPause(char c)
	{
		return c == 'p' || c == 'P';
	}

	private static boolean isToneWait(char c)
	{
		return c == 'w' || c == 'W';
	}

	/** Returns true if ch is not dialable or alpha char */
	private static boolean isSeparator(char ch)
	{
		return !isDialable(ch) && !(('a' <= ch && ch <= 'z') || ('A' <= ch && ch <= 'Z'));
	}

	/**
	 * Normalize a phone number by removing the characters other than digits. If
	 * the given number has keypad letters, the letters will be converted to
	 * digits first.
	 * 
	 * @param phoneNumber
	 *            the number to be normalized.
	 * @return the normalized number.
	 * 
	 * @hide
	 */
	public static String normalizeNumber(String phoneNumber)
	{
		StringBuilder sb = new StringBuilder();
		int len = phoneNumber.length();
		for (int i = 0; i < len; i++)
		{
			char c = phoneNumber.charAt(i);
			// Character.digit() supports ASCII and Unicode digits (fullwidth,
			// Arabic-Indic, etc.)
			int digit = Character.digit(c, 10);
			if (digit != -1)
			{
				sb.append(digit);
			}
			else if (i == 0 && c == '+')
			{
				sb.append(c);
			}
			else if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
			{
				return normalizeNumber(PhoneNumberUtils.convertKeypadLettersToDigits(phoneNumber));
			}
		}
		return sb.toString();
	}

	/**
	 * Translates any alphabetic letters (i.e. [A-Za-z]) in the specified phone
	 * number into the equivalent numeric digits, according to the phone keypad
	 * letter mapping described in ITU E.161 and ISO/IEC 9995-8.
	 * 
	 * @return the input string, with alpha letters converted to numeric digits
	 *         using the phone keypad letter mapping. For example, an input of
	 *         "1-800-GOOG-411" will return "1-800-4664-411".
	 */
	public static String convertKeypadLettersToDigits(String input)
	{
		if (input == null)
		{
			return input;
		}
		int len = input.length();
		if (len == 0)
		{
			return input;
		}

		char[] out = input.toCharArray();

		for (int i = 0; i < len; i++)
		{
			char c = out[i];
			// If this char isn't in KEYPAD_MAP at all, just leave it alone.
			out[i] = (char) KEYPAD_MAP.get(c, c);
		}

		return new String(out);
	}

	/**
	 * The phone keypad letter mapping (see ITU E.161 or ISO/IEC 9995-8.) TODO:
	 * This should come from a resource.
	 */
	private static final SparseIntArray	KEYPAD_MAP	= new SparseIntArray();
	static
	{
		KEYPAD_MAP.put('a', '2');
		KEYPAD_MAP.put('b', '2');
		KEYPAD_MAP.put('c', '2');
		KEYPAD_MAP.put('A', '2');
		KEYPAD_MAP.put('B', '2');
		KEYPAD_MAP.put('C', '2');

		KEYPAD_MAP.put('d', '3');
		KEYPAD_MAP.put('e', '3');
		KEYPAD_MAP.put('f', '3');
		KEYPAD_MAP.put('D', '3');
		KEYPAD_MAP.put('E', '3');
		KEYPAD_MAP.put('F', '3');

		KEYPAD_MAP.put('g', '4');
		KEYPAD_MAP.put('h', '4');
		KEYPAD_MAP.put('i', '4');
		KEYPAD_MAP.put('G', '4');
		KEYPAD_MAP.put('H', '4');
		KEYPAD_MAP.put('I', '4');

		KEYPAD_MAP.put('j', '5');
		KEYPAD_MAP.put('k', '5');
		KEYPAD_MAP.put('l', '5');
		KEYPAD_MAP.put('J', '5');
		KEYPAD_MAP.put('K', '5');
		KEYPAD_MAP.put('L', '5');

		KEYPAD_MAP.put('m', '6');
		KEYPAD_MAP.put('n', '6');
		KEYPAD_MAP.put('o', '6');
		KEYPAD_MAP.put('M', '6');
		KEYPAD_MAP.put('N', '6');
		KEYPAD_MAP.put('O', '6');

		KEYPAD_MAP.put('p', '7');
		KEYPAD_MAP.put('q', '7');
		KEYPAD_MAP.put('r', '7');
		KEYPAD_MAP.put('s', '7');
		KEYPAD_MAP.put('P', '7');
		KEYPAD_MAP.put('Q', '7');
		KEYPAD_MAP.put('R', '7');
		KEYPAD_MAP.put('S', '7');

		KEYPAD_MAP.put('t', '8');
		KEYPAD_MAP.put('u', '8');
		KEYPAD_MAP.put('v', '8');
		KEYPAD_MAP.put('T', '8');
		KEYPAD_MAP.put('U', '8');
		KEYPAD_MAP.put('V', '8');

		KEYPAD_MAP.put('w', '9');
		KEYPAD_MAP.put('x', '9');
		KEYPAD_MAP.put('y', '9');
		KEYPAD_MAP.put('z', '9');
		KEYPAD_MAP.put('W', '9');
		KEYPAD_MAP.put('X', '9');
		KEYPAD_MAP.put('Y', '9');
		KEYPAD_MAP.put('Z', '9');
	}

	/**
	 * Compare phone numbers a and b, return true if they're identical enough
	 * for caller ID purposes.
	 * 
	 * - Compares from right to left - requires MIN_MATCH (7) characters to
	 * match - handles common trunk prefixes and international prefixes
	 * (basically, everything except the Russian trunk prefix)
	 * 
	 * Note that this method does not return false even when the two phone
	 * numbers are not exactly same; rather; we can call this method
	 * "similar()", not "equals()".
	 * 
	 * @hide
	 */
	public static boolean compareLoosely(String a, String b)
	{
		int ia, ib;
		int matched;
		int numNonDialableCharsInA = 0;
		int numNonDialableCharsInB = 0;

		if (a == null || b == null)
			return a == b;

		if (a.length() == 0 || b.length() == 0)
		{
			return false;
		}

		ia = indexOfLastNetworkChar(a);
		ib = indexOfLastNetworkChar(b);
		matched = 0;

		while (ia >= 0 && ib >= 0)
		{
			char ca, cb;
			boolean skipCmp = false;

			ca = a.charAt(ia);

			if (!isDialable(ca))
			{
				ia--;
				skipCmp = true;
				numNonDialableCharsInA++;
			}

			cb = b.charAt(ib);

			if (!isDialable(cb))
			{
				ib--;
				skipCmp = true;
				numNonDialableCharsInB++;
			}

			if (!skipCmp)
			{
				if (cb != ca && ca != WILD && cb != WILD)
				{
					break;
				}
				ia--;
				ib--;
				matched++;
			}
		}

		if (matched < MIN_MATCH)
		{
			int effectiveALen = a.length() - numNonDialableCharsInA;
			int effectiveBLen = b.length() - numNonDialableCharsInB;

			// if the number of dialable chars in a and b match, but the matched
			// chars < MIN_MATCH,
			// treat them as equal (i.e. 404-04 and 40404)
			if (effectiveALen == effectiveBLen && effectiveALen == matched)
			{
				return true;
			}

			return false;
		}

		// At least one string has matched completely;
		if (matched >= MIN_MATCH && (ia < 0 || ib < 0))
		{
			return true;
		}

		/*
		 * Now, what remains must be one of the following for a match:
		 * 
		 * - a '+' on one and a '00' or a '011' on the other - a '0' on one and
		 * a (+,00)<country code> on the other (for this, a '0' and a '00'
		 * prefix would have succeeded above)
		 */

		if (matchIntlPrefix(a, ia + 1) && matchIntlPrefix(b, ib + 1))
		{
			return true;
		}

		if (matchTrunkPrefix(a, ia + 1) && matchIntlPrefixAndCC(b, ib + 1))
		{
			return true;
		}

		if (matchTrunkPrefix(b, ib + 1) && matchIntlPrefixAndCC(a, ia + 1))
		{
			return true;
		}

		return false;
	}

	/**
	 * index of the last character of the network portion (eg anything after is
	 * a post-dial string)
	 */
	static private int indexOfLastNetworkChar(String a)
	{
		int pIndex, wIndex;
		int origLength;
		int trimIndex;

		origLength = a.length();

		pIndex = a.indexOf(PAUSE);
		wIndex = a.indexOf(WAIT);

		trimIndex = minPositive(pIndex, wIndex);

		if (trimIndex < 0)
		{
			return origLength - 1;
		}
		else
		{
			return trimIndex - 1;
		}
	}

	/** or -1 if both are negative */
	static private int minPositive(int a, int b)
	{
		if (a >= 0 && b >= 0)
		{
			return (a < b) ? a : b;
		}
		else if (a >= 0)
		{ /* && b < 0 */
			return a;
		}
		else if (b >= 0)
		{ /* && a < 0 */
			return b;
		}
		else
		{ /* a < 0 && b < 0 */
			return -1;
		}
	}

	/**
	 * Phone numbers are stored in "lookup" form in the database as reversed
	 * strings to allow for caller ID lookup
	 * 
	 * This method takes a phone number and makes a valid SQL "LIKE" string that
	 * will match the lookup form
	 * 
	 */
	/**
	 * all of a up to len must be an international prefix or
	 * separators/non-dialing digits
	 */
	private static boolean matchIntlPrefix(String a, int len)
	{
		/* '([^0-9*#+pwn]\+[^0-9*#+pwn] | [^0-9*#+pwn]0(0|11)[^0-9*#+pwn] )$' */
		/* 0 1 2 3 45 */

		int state = 0;
		for (int i = 0; i < len; i++)
		{
			char c = a.charAt(i);

			switch (state)
			{
				case 0:
					if (c == '+')
						state = 1;
					else if (c == '0')
						state = 2;
					else if (isNonSeparator(c))
						return false;
					break;

				case 2:
					if (c == '0')
						state = 3;
					else if (c == '1')
						state = 4;
					else if (isNonSeparator(c))
						return false;
					break;

				case 4:
					if (c == '1')
						state = 5;
					else if (isNonSeparator(c))
						return false;
					break;

				default:
					if (isNonSeparator(c))
						return false;
					break;

			}
		}

		return state == 1 || state == 3 || state == 5;
	}

	/**
	 * all of 'a' up to len must be a (+|00|011)country code) We're fast and
	 * loose with the country code. Any \d{1,3} matches
	 */
	private static boolean matchIntlPrefixAndCC(String a, int len)
	{
		/* [^0-9*#+pwn]*(\+|0(0|11)\d\d?\d? [^0-9*#+pwn] $ */
		/* 0 1 2 3 45 6 7 8 */

		int state = 0;
		for (int i = 0; i < len; i++)
		{
			char c = a.charAt(i);

			switch (state)
			{
				case 0:
					if (c == '+')
						state = 1;
					else if (c == '0')
						state = 2;
					else if (isNonSeparator(c))
						return false;
					break;

				case 2:
					if (c == '0')
						state = 3;
					else if (c == '1')
						state = 4;
					else if (isNonSeparator(c))
						return false;
					break;

				case 4:
					if (c == '1')
						state = 5;
					else if (isNonSeparator(c))
						return false;
					break;

				case 1:
				case 3:
				case 5:
					if (isISODigit(c))
						state = 6;
					else if (isNonSeparator(c))
						return false;
					break;

				case 6:
				case 7:
					if (isISODigit(c))
						state++;
					else if (isNonSeparator(c))
						return false;
					break;

				default:
					if (isNonSeparator(c))
						return false;
			}
		}

		return state == 6 || state == 7 || state == 8;
	}

	/** all of 'a' up to len must match non-US trunk prefix ('0') */
	private static boolean matchTrunkPrefix(String a, int len)
	{
		boolean found;

		found = false;

		for (int i = 0; i < len; i++)
		{
			char c = a.charAt(i);

			if (c == '0' && !found)
			{
				found = true;
			}
			else if (isNonSeparator(c))
			{
				return false;
			}
		}

		return found;
	}
}
