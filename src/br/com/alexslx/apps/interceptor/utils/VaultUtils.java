/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.interceptor.utils;

import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

import br.com.alexslx.apps.interceptor.models.Contact;

import android.annotation.SuppressLint;

public class VaultUtils implements Comparator<Contact>
{

	static final String	PRIVATE_TAG	= "VaultUtils";

	@SuppressLint("SimpleDateFormat")
	public static String getDateFromLong(long longdate)
	{
		Date date = new Date(longdate);
		SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy HH:mm");

		return fmt.format(date);
	}

	public static int getRandomNumber(int min, int max)
	{
		int n = min + (int) (Math.random() * ((max - min) + 1));
		return n;
	}

	public static String getRandomString(int min, int max)
	{
		int n = getRandomNumber(min, max);
		return String.valueOf(n);
	}

	public int compare(Contact c1, Contact c2)
	{
		return c1.getContact().getDisplayName().compareToIgnoreCase(c2.getContact().getDisplayName());
	}
}
