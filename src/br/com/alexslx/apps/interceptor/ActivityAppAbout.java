/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.interceptor;

import android.os.Bundle;
import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class ActivityAppAbout extends Activity
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
        setContentView(R.layout.activity_about);
        
		try
		{
			//TextView aboutAppName		= (TextView)findViewById(R.id.aboutAppName);
			TextView aboutAppVersion	= (TextView)findViewById(R.id.aboutAppVersion);
			PackageManager manager 		= getPackageManager();
			PackageInfo info 			= manager.getPackageInfo(getApplicationContext().getPackageName(), 0);
			aboutAppVersion.setText( info.versionName );
		}
		catch (NameNotFoundException e)
		{
			//
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		return false;
	}
	
	public void onClick(View v)
	{
		finish();
	}

}
