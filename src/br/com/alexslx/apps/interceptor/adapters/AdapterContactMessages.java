/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.interceptor.adapters;

import java.util.ArrayList;

import br.com.alexslx.apps.interceptor.R;
import br.com.alexslx.apps.interceptor.models.ContactMessage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class AdapterContactMessages extends BaseAdapter
{

	private LayoutInflater				mInflater;
	private ArrayList<ContactMessage>	items;

	public AdapterContactMessages(Context context, ArrayList<ContactMessage> items)
	{
		this.items = items;
		mInflater = LayoutInflater.from(context);
	}

	/**
	 * 
	 * 
	 * @return
	 */
	public int getCount()
	{
		return items.size();
	}

	/**
	 * 
	 * 
	 * @param position
	 * @return
	 */
	public ContactMessage getItem(int position)
	{
		return items.get(position);
	}

	/**
	 * 
	 * 
	 * @param position
	 * @return
	 */
	public long getItemId(int position)
	{
		return position;
	}

	@SuppressLint("SimpleDateFormat")
	public View getView(int position, View view, ViewGroup parent)
	{
		ContactMessage msg = items.get(position);
		view = mInflater.inflate(R.layout.message_row, null);

		RelativeLayout wrapper = (RelativeLayout) view.findViewById(R.id.wrapper);
		TextView messageRow = (TextView) view.findViewById(R.id.message);
		// TextView messageDate =
		// (TextView)view.findViewById(R.id.message_date);

		messageRow.setText(msg.getMessage());
		messageRow.setBackgroundResource(msg.isFromContact() ? R.drawable.bubble_yellow : R.drawable.bubble_green);

		/*
		 * Date date = new Date( msg.date ); SimpleDateFormat fmt = new
		 * SimpleDateFormat("hh:mm"); messageDate.setText( fmt.format(date) );
		 */

		wrapper.setGravity(msg.isFromContact() ? Gravity.LEFT : Gravity.RIGHT);

		return view;
	}

	public Bitmap decodeToBitmap(byte[] decodedByte)
	{
		return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
	}
}
