/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.interceptor.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.com.alexslx.apps.interceptor.R;
import br.com.alexslx.apps.interceptor.models.Contact;

public class AdapterVaultListView extends BaseAdapter
{

	private LayoutInflater		mInflater;
	private ArrayList<Contact>	items;

	public AdapterVaultListView(Context context, ArrayList<Contact> items)
	{
		this.items = items;
		mInflater = LayoutInflater.from(context);
	}

	/**
	 * 
	 * 
	 * @return
	 */
	public int getCount()
	{
		return items.size();
	}

	/**
	 * 
	 * 
	 * @param position
	 * @return
	 */
	public Contact getItem(int position)
	{
		return items.get(position);
	}

	/**
	 * 
	 * 
	 * @param position
	 * @return
	 */
	public long getItemId(int position)
	{
		return position;
	}

	public View getView(int position, View view, ViewGroup parent)
	{
		Contact c = items.get(position);
		view = mInflater.inflate(R.layout.contact_row, null);

		((TextView) view.findViewById(R.id.displayName)).setText(c.getContact().getDisplayName());
		((TextView) view.findViewById(R.id.contactInfos)).setText(c.getContact().getInfos());

		return view;
	}
}
