/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.interceptor.adapters;

import java.util.ArrayList;

import br.com.alexslx.apps.interceptor.R;
import br.com.alexslx.apps.interceptor.models.ContactHistory;
import br.com.alexslx.apps.interceptor.utils.VaultUtils;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterContactHistory extends BaseAdapter
{

	private LayoutInflater				mInflater;
	private ArrayList<ContactHistory>	items;

	public AdapterContactHistory(Context context, ArrayList<ContactHistory> items)
	{
		this.items = items;
		mInflater = LayoutInflater.from(context);
	}

	/**
	 * 
	 * 
	 * @return
	 */
	public int getCount()
	{
		return items.size();
	}

	/**
	 * 
	 * 
	 * @param position
	 * @return
	 */
	public ContactHistory getItem(int position)
	{
		return items.get(position);
	}

	/**
	 * 
	 * 
	 * @param position
	 * @return
	 */
	public long getItemId(int position)
	{
		return position;
	}

	public View getView(int position, View view, ViewGroup parent)
	{
		if (view == null)
		{
			view = mInflater.inflate(R.layout.history_row, null);
		}
		final ContactHistory obj = items.get(position);

		TextView callDetailNumber = (TextView) view.findViewById(R.id.callDetailNumber);
		TextView callDetailEvent = (TextView) view.findViewById(R.id.callDetailEvent);
		TextView callDetailTime = (TextView) view.findViewById(R.id.callDetailTime);
		ImageView callDetailType = (ImageView) view.findViewById(R.id.callDetailType);
		ImageView callBackBtn = (ImageView) view.findViewById(R.id.callBackBtn);
		Resources res = view.getResources();

		callDetailNumber.setText(obj.getNumber());
		callDetailTime.setText(VaultUtils.getDateFromLong(obj.getDate()));
		callBackBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v)
			{
				Intent intent = new Intent(Intent.ACTION_CALL);
				intent.setData(Uri.parse("tel:" + obj.getNumber()));
				v.getContext().startActivity(intent);
			}

		});

		String[] types = res.getStringArray(R.array.contact_history_types);
		callDetailEvent.setText(types[obj.getType()]);
		switch (obj.getType())
		{
			case ContactHistory.TYPE_CALL_OUTGOING:
				callDetailType.setImageDrawable(view.getResources().getDrawable(android.R.drawable.sym_call_outgoing));
				break;
			case ContactHistory.TYPE_CALL_INCOMING:
				callDetailType.setImageDrawable(view.getResources().getDrawable(android.R.drawable.sym_call_incoming));
				break;
			case ContactHistory.TYPE_CALL_MISSED:
				callDetailType.setImageDrawable(view.getResources().getDrawable(android.R.drawable.sym_call_missed));
				break;
			case ContactHistory.TYPE_SMS_OUTGOING:
				callDetailType.setImageDrawable(view.getResources().getDrawable(android.R.drawable.sym_action_chat));
				break;
			case ContactHistory.TYPE_SMS_INCOMING:
				callDetailType.setImageDrawable(view.getResources().getDrawable(android.R.drawable.sym_action_chat));
				break;
		}

		return view;
	}
}
