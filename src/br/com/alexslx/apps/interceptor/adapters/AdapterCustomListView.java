/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.interceptor.adapters;

import java.util.ArrayList;

import br.com.alexslx.apps.interceptor.R;
import br.com.alexslx.apps.interceptor.models.CustomListEntryItem;
import br.com.alexslx.apps.interceptor.models.CustomListItem;
import br.com.alexslx.apps.interceptor.models.CustomListSectionItem;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterCustomListView extends BaseAdapter
{

	private LayoutInflater				mInflater;
	private ArrayList<CustomListItem>	items;

	public AdapterCustomListView(Context context, ArrayList<CustomListItem> items)
	{
		this.items = items;
		mInflater = LayoutInflater.from(context);
	}

	/**
	 * 
	 * 
	 * @return
	 */
	public int getCount()
	{
		return items.size();
	}

	/**
	 * 
	 * 
	 * @param position
	 * @return
	 */
	public CustomListItem getItem(int position)
	{
		return items.get(position);
	}

	/**
	 * 
	 * 
	 * @param position
	 * @return
	 */
	public long getItemId(int position)
	{
		return position;
	}

	public View getView(int position, View view, ViewGroup parent)
	{
		View v = view;
		final CustomListItem i = items.get(position);
		if (i == null)
			return v;

		if (i.isSection())
		{
			CustomListSectionItem item = (CustomListSectionItem) i;
			v = mInflater.inflate(R.layout.customlist_section, null);

			v.setOnClickListener(null);
			v.setOnLongClickListener(null);
			v.setLongClickable(false);

			final TextView sectionTitle = (TextView) v.findViewById(R.id.customlist_section_text);
			sectionTitle.setText(item.getTitle());
		}
		else
		{
			CustomListEntryItem item = (CustomListEntryItem) i;
			if (item.getResourceId() == 0)
				v = mInflater.inflate(R.layout.customlist_entry_row, null);
			else
				v = mInflater.inflate(item.getResourceId(), null);

			item.fillView(v);

			// Icon and Action
			ImageView actionImage = (ImageView) v.findViewById(R.id.customListRowActionIcon);
			if (item.withIcon() == false)
			{
				actionImage.setVisibility(View.GONE);
			}
			else
			{
				actionImage.setImageResource(item.getIconResource());
				if (item.getIconListener() != null)
					actionImage.setOnClickListener(item.getIconListener());
			}

			// Remove border when last element of section
			boolean removeBorder = (items.size() == (position + 1)) ? true : items.get(position + 1).isSection();
			if (removeBorder)
				((ImageView) v.findViewById(R.id.customListRowBorder)).setVisibility(View.GONE);
		}

		return v;
	}
}
