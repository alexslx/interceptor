/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.interceptor.receivers;

import java.lang.reflect.Method;
import java.util.Date;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import br.com.alexslx.apps.interceptor.models.Contact;
import br.com.alexslx.apps.interceptor.models.ContactHistory;
import br.com.alexslx.apps.interceptor.utils.L;

import com.android.internal.telephony.ITelephony;

public class ReceiverIncomingCalls extends BroadcastReceiver
{

	@Override
	public void onReceive(Context context, Intent intent)
	{
		Bundle extras = intent.getExtras();
		if (extras == null)
			return;

		String state = extras.getString(TelephonyManager.EXTRA_STATE);
		if (state == null)
			return;

		L.d("Starting receiver... " + new Date().getTime());

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

		if (state.equals(TelephonyManager.EXTRA_STATE_RINGING))
		{
			String callerNumber = extras.getString(TelephonyManager.EXTRA_INCOMING_NUMBER);

			L.d("Phone is ringing from " + callerNumber + "!");

			Contact c = Contact.getContactByNumber(callerNumber);
			if (c != null)
			{
				L.d("Found a match! displayName is: " + c.getContact().getDisplayName());
				if (c.getContact().optCallHide)
				{
					L.d("Decline and hide this call!");

					declineCall(context);
					ContactHistory.newHistoryEntry(context, c.getContact().getID(), ContactHistory.TYPE_CALL_MISSED, callerNumber, true);
					
					if(c.getContact().optCallAlert)
					{
						c.sendNotification(context, true);
					}
				}
			}
		}
		else if (state.equals(TelephonyManager.EXTRA_STATE_IDLE))
		{
			if (prefs.getBoolean("log_clear", false) == false)
				return;

			String callerNumber = prefs.getString("log_number", "-1");
			String callerId = prefs.getString("log_id", "-1");

			if (!callerId.equalsIgnoreCase("-1"))
			{
				L.d("Saving internal log!");

				ContactHistory.newHistoryEntry(context, callerId, ContactHistory.TYPE_CALL_OUTGOING, callerNumber, true);
			}

			Intent i = new Intent(context, ServiceIncomingCall.class);
			context.startService(i);

			// Revert
			prefs.edit().remove("log_clear").commit();
			prefs.edit().remove("log_number").commit();
			prefs.edit().remove("log_id").commit();
		}
		L.d("Finishing receiver... " + new Date().getTime());
	}

	@SuppressWarnings({ "unused", "deprecation" })
	private void ignoreCall(Context context)
	{
		ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		am.restartPackage("com.android.providers.telephony");
		am.restartPackage("com.android.phone");
	}

	@SuppressWarnings("unused")
	private void turnScreenOff(Context context)
	{
		Intent buttonIntent = new Intent(Intent.ACTION_MEDIA_BUTTON);
		buttonIntent.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_POWER));

		context.sendOrderedBroadcast(buttonIntent, "android.permission.CALL_PRIVILEGED");
	}

	// SecurityException
	@SuppressWarnings("unused")
	private void answerCall(Context context)
	{
		ITelephony telephonyService = getTelephonyService(context);
		try
		{
			telephonyService.answerRingingCall();
		}
		catch (Exception e)
		{
			L.d("Error trying to answer call with ITelephony. Fallback to headset.");
			answerCallByHeadset(context);
		}
	}

	private void answerCallByHeadset(Context context)
	{
		Intent buttonIntent = new Intent(Intent.ACTION_MEDIA_BUTTON);
		int currentAPI = android.os.Build.VERSION.SDK_INT;
		if (currentAPI > android.os.Build.VERSION_CODES.FROYO)
			buttonIntent.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_HEADSETHOOK));
		else
			buttonIntent.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_HEADSETHOOK));

		context.sendOrderedBroadcast(buttonIntent, "android.permission.CALL_PRIVILEGED");
	}

	private void declineCall(Context context)
	{
		AudioManager audioManager 	= (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		int oldRingerMode			= audioManager.getRingerMode();
		
		audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
		
		//audioManager.setStreamSolo(AudioManager.STREAM_RING, true);
		//audioManager.setStreamMute(AudioManager.STREAM_RING, true);
		
		ITelephony telephonyService = getTelephonyService(context);
		try
		{
			telephonyService.endCall();
		}
		catch (Exception e)
		{
			L.d("Error trying to decline a call using ITelephony service.");
			turnScreenOff(context);
		}
		
		audioManager.setRingerMode(oldRingerMode);
		//audioManager.setStreamSolo(AudioManager.STREAM_RING, false);
		//audioManager.setStreamMute(AudioManager.STREAM_RING, false);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	ITelephony getTelephonyService(Context context)
	{
		TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		try
		{
			Class myClass = Class.forName(telephonyManager.getClass().getName());
			Method method = myClass.getDeclaredMethod("getITelephony");
			method.setAccessible(true);

			ITelephony telephonyService = (ITelephony) method.invoke(telephonyManager);

			return telephonyService;
		}
		catch (Exception e)
		{
			L.d("Error trying to get ITelephony");
			return null;
		}
	}
}
