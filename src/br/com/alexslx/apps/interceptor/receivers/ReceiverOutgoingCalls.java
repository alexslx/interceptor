/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.interceptor.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.widget.Toast;
import br.com.alexslx.apps.interceptor.R;
import br.com.alexslx.apps.interceptor.models.Contact;
import br.com.alexslx.apps.interceptor.utils.L;

public class ReceiverOutgoingCalls extends BroadcastReceiver
{

	@Override
	public void onReceive(Context context, Intent intent)
	{
		String receivedNumber = getResultData() != null ? getResultData() : intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
		if (receivedNumber == null)
			return;

		L.d("Receiving request to place a call to " + receivedNumber);

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		String appFormatedCode = String.format("*#%s#", prefs.getString("app_code", "8520"));
		String appFormatedCode2 = String.format("**%s*", prefs.getString("app_code", "8520"));

		// FIXME: Take a look on fillListView of VaultContact and you'll see why
		// i have appFormatedCode2.
		if (receivedNumber.contains(appFormatedCode) || (receivedNumber.contains(appFormatedCode2) && receivedNumber.length() > (appFormatedCode.length() + 1)))
		{
			L.d("Our code has been used!");

			if (receivedNumber.equalsIgnoreCase(appFormatedCode))
			{
				L.d("Code used to open our application!");

				PackageManager manager = context.getPackageManager();
				Intent i = new Intent();
				i = manager.getLaunchIntentForPackage(context.getApplicationContext().getPackageName());
				i.addCategory(Intent.CATEGORY_LAUNCHER);
				context.startActivity(i);

				setResultData(null);
			}
			else
			{
				String rest = receivedNumber.substring(appFormatedCode.length());
				if (rest.length() > 1)
				{
					L.d("Place call without log to " + rest);

					prefs.edit().putBoolean("log_clear", true).commit();
					prefs.edit().putString("log_number", rest).commit();
					setResultData(rest);
				}
				else
				{
					int i = Integer.valueOf(rest);
					switch (i)
					{
						case 0:
							L.d("Application disabled!");
							prefs.edit().putBoolean("app_active", false).commit();
							Toast.makeText(context, context.getString(R.string.app_disabled), Toast.LENGTH_SHORT).show();
							break;
						case 1:
							L.d("Application enabled!");
							prefs.edit().putBoolean("app_active", true).commit();
							Toast.makeText(context, context.getString(R.string.app_enabled), Toast.LENGTH_SHORT).show();
							break;
						case 5:
							L.d("Toggle silence mode!");
							boolean toggle = !prefs.getBoolean("app_silence", false);
							prefs.edit().putBoolean("app_silence", toggle).commit();
							Vibrator vib = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
							if(toggle)
								vib.vibrate(300);
							else
								vib.vibrate(new long[] {0,300,100,300,100,300}, -1);
							break;
					}
					setResultData(null);
				}
			}
		}
		else
		{
			if (prefs.getBoolean("app_active", true) == false)
				return;

			Contact c = Contact.getContactByNumber(receivedNumber);
			if (c != null)
			{
				L.d("Found a match! displayName is: " + c.getContact().getDisplayName());
				if (c.getContact().optCallBlock)
				{
					L.d("Blocking outgoing call to " + c.getContact().getDisplayName());
					setResultData(null);
				}
				else
				{
					L.d("Call placed. Our mission is clear the log after call ends.");

					prefs.edit().putBoolean("log_clear", true).commit();
					prefs.edit().putString("log_number", receivedNumber).commit();
					prefs.edit().putString("log_id", c.getContact().getID()).commit();
				}
			}
		}
	}
}
