/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.interceptor.receivers;

import java.util.Date;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import br.com.alexslx.apps.interceptor.models.Contact;
import br.com.alexslx.apps.interceptor.models.ContactMessage;
import br.com.alexslx.apps.interceptor.utils.L;

public class ReceiverMessages extends BroadcastReceiver
{

	@Override
	public void onReceive(Context context, Intent intent)
	{
		Bundle extras = intent.getExtras();
		Object[] pdus = (Object[]) extras.get("pdus");
		SmsMessage sms;
		boolean abortBroadcast = false;

		this.abortBroadcast();

		L.d("Starting receiver... " + new Date().getTime());
		for (Object pdu : pdus)
		{
			sms = SmsMessage.createFromPdu((byte[]) pdu);
			String callerNumber = sms.getOriginatingAddress();

			L.d("Received a message from " + callerNumber);

			Contact c = Contact.getContactByNumber(callerNumber);
			if (c != null)
			{
				L.d("Found a match! displayName is: " + c.getContact().getDisplayName());
				if (c.getContact().optContactHide)
				{
					L.d("Hidding message from this contact!");
					abortBroadcast = true;
					ContactMessage.newMessageEntry(context, c.getContact().getID(), sms.getDisplayMessageBody(), true, true);
					
					if(c.getContact().optMessagesAlert)
					{
						c.sendNotification(context, false);
					}
				}
			}
		}
		L.d("Finishing receiver... " + new Date().getTime());

		if (!abortBroadcast)
			this.clearAbortBroadcast();
	}
}
