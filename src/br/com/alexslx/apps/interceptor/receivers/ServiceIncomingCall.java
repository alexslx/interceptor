/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.interceptor.receivers;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog.Calls;
import android.telephony.TelephonyManager;
import br.com.alexslx.apps.interceptor.utils.L;

public class ServiceIncomingCall extends IntentService
{

	public ServiceIncomingCall()
	{
		super("ServiceIncomingCall");
	}

	@Override
	protected void onHandleIntent(Intent intent)
	{
		Context context = getBaseContext();
		TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

		if (tm.getCallState() != TelephonyManager.CALL_STATE_IDLE)
			return;

		try
		{
			L.d("Sleeping for five seconds...");
			Thread.sleep(5000);

			L.d("Trying to delete last call log");
			Cursor callLogCursor = context.getContentResolver().query(Calls.CONTENT_URI, null, null, null, Calls.DATE + " DESC");
			if (callLogCursor.moveToFirst())
			{
				L.d("Deleting last call log");
				int id = callLogCursor.getInt(callLogCursor.getColumnIndex(Calls._ID));
				context.getContentResolver().delete(Uri.withAppendedPath(Calls.CONTENT_URI, String.valueOf(id)), "", null);
			}
		}
		catch (Exception e)
		{
			L.d("Exception: " + e.getMessage());
		}

		L.d("Finishing task");
	}
}
