/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.interceptor;

import java.util.ArrayList;

import br.com.alexslx.apps.interceptor.models.Contact;
import br.com.alexslx.apps.interceptor.utils.L;

import com.orm.SugarApp;

public class ApplicationHelper extends SugarApp
{

	private ArrayList<Contact>	contactList;
	private Contact				_contact;

	public ApplicationHelper()
	{
		super();

		L.d("Initializing App");

		this.contactList = new ArrayList<Contact>();
		this._contact = null;

	}

	public Contact getContact()
	{
		return this._contact;
	}

	public void setContact(Contact c)
	{
		this._contact = c;
	}

	public ArrayList<Contact> getContactList()
	{
		return this.contactList;
	}

	public Contact getContactByPosition(int position)
	{
		if (this.contactList == null || this.contactList.size() <= position)
			return null;

		return this.contactList.get(position);
	}
}
