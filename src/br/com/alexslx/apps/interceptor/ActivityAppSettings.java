/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.interceptor;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;

public class ActivityAppSettings extends PreferenceActivity
{
	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
		Preference marketPref = (Preference) findPreference("app_market");
		Preference aboutPref = (Preference) findPreference("app_about");

		aboutPref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			public boolean onPreferenceClick(Preference preference)
			{
				Intent i = new Intent(getBaseContext(), ActivityAppAbout.class);
				startActivity(i);
				return true;
			}
		});

		marketPref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			public boolean onPreferenceClick(Preference preference)
			{
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse("market://details?id=" + getApplicationContext().getPackageName()));
				startActivity(i);
				return true;
			}
		});
	}
}
