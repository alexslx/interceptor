package com.android.internal.telephony; 

interface ITelephony {
	void dial(String number);
	void call(String number);
	boolean showCallScreen();
	boolean showCallScreenWithDialpad(boolean showDialpad);
	boolean endCall();
	void answerRingingCall();
}